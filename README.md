# MRT\#

MRT# is a fast multi-threaded MRT parser library written in C#. 

It is freely available on [NuGet](https://www.nuget.org/packages/MRTSharp/).

## Publication
- **MRT#: a Fast Multi-Threaded MRT Parser** (at **IM 2021**)
    - [Paper](http://dl.ifip.org/db/conf/im/im2021demo/213293.pdf)
    - [Presentation](https://www.youtube.com/watch?v=ijGGX4EpmeI)

Winner of the **"IM 2021 Best Demonstration Award"** ([Link](https://im2021.ieee-im.org/program/awards))


## Example Usage

The entrypoint of the parser is the `Run` static method of the `MRTParser` class.

```csharp
using MRTSharp;
using System.IO;

class Program
{
    static void Main()
    {
        FileInfo file = new FileInfo(@"path-to-mrt-file");

        MRTParser.Run(file, (mrtEntry) =>
        {
            // Do something with the mrtEntry
        });
    }
}
```

### Other options

#### Input file options
`MRTParser.Run` method has different signatures. It accepts `FileInfo`, `IEnumerable<FileInfo>` or `DirectoryInfo` as parameter. 
Using the `DirectoryInfo` version, all files contained in the specified directory are processed, ignoring subdirectories and their files.

The file can be a plain MRT file or a compressed one. The compression format is automatically detected through the file extension. Accepted extensions for compressed files are `.gz` and `.bz2`.

#### Processing options
All the `MRTParser.Run` methods that accept more than one file (`IEnumerable<FileInfo>` or `DirectoryInfo`) have an optional boolean parameter `multithreaded` (default=true) that can be used to disable the multithreaded parsing.

#### Output options
`MRTParser.Run` method has two signatures. 
The first one, only takes a single parameter (`FileInfo`, `IEnumerable<FileInfo>` or `DirectoryInfo`) and returns a `List`.
The second one, takes two parameters: `FileInfo`/`IEnumerable<FileInfo>`/`DirectoryInfo`, and a callback (like in the above example) that enables the user to further process MRT records.

Moreover, both the one and two parameter versions of the method can be invoked with the diamond operator `<T>`. Using `<T>`, it is possible to specify the type of the returned objects. All the objects that cannot be downcasted to `T` are automatically discarded.
`T` can be one of the following: `MRTEntry` (default, if not specified), `TableDumpRIBRecord`, `BGP4MPEntry`, `BGP4MPMessage`, or `BGP4MPStateChange`.

#### Logging

MRT# logs on the `DEBUG` channel if an MRT file is corrupted or some fields cannot be parsed.

You can connect your own logger, that MUST be an instance of a `Microsoft.Extensions.Logging.ILogger` implementation, using the `SetLogger` static method of `MRTParser` before calling the `Run` method.

```csharp
MRTParser.SetLogger(yourILoggerVariable);

MRTParser.Run(...);
```

## Support

In the following, there is a list of what MRT# is currently supporting.

### MRT Types

Defined [here](https://www.iana.org/assignments/mrt/mrt.xhtml#type-codes)

| Name              | Value |
|-------------------|-------|
| ~~TABLE_DUMP~~    | 12    |
| TABLE_DUMP_V2     | 13    |
| BGP4MP            | 16    |
| BGP4MP_ET         | 17    |

### TABLE_DUMP_V2 Subtype

Defined [here](https://www.iana.org/assignments/mrt/mrt.xhtml#table-dump-v2-subtype-codes)

| Name                               | Value |
|------------------------------------|-------|
| PEER_INDEX_TABLE                   | 1     |
| RIB_IPV4_UNICAST                   | 2     |
| ~~RIB_IPV4_MULTICAST~~             | 3     |
| RIB_IPV6_UNICAST                   | 4     |
| ~~RIB_IPV6_MULTICAST~~             | 5     |
| ~~RIB_GENERIC~~                    | 6     |
| ~~GEO_PEER_TABLE~~                 | 7     |
| RIB_IPV4_UNICAST_ADDPATH           | 8     |
| ~~RIB_IPV4_MULTICAST_ADDPATH~~     | 9     |
| RIB_IPV6_UNICAST_ADDPATH           | 10    |
| ~~RIB_IPV6_MULTICAST_ADDPATH~~     | 11    |
| ~~RIB_GENERIC_ADDPATH~~            | 12    |

### BGP4MP/BGP4MP_ET Subtype

Defined [here](https://www.iana.org/assignments/mrt/mrt.xhtml#BGP4MP-codes) and [here](https://www.iana.org/assignments/mrt/mrt.xhtml#BGP4MP_ET-codes) 

| Name                                 | Value |
|--------------------------------------|-------|
| BGP4MP_STATE_CHANGE                  | 0     |
| BGP4MP_MESSAGE                       | 1     |
| BGP4MP_MESSAGE_AS4                   | 4     |
| BGP4MP_MESSAGE_LOCAL                 | 6     |
| BGP4MP_MESSAGE_AS4_LOCAL             | 7     |
| BGP4MP_MESSAGE_ADDPATH               | 8     |
| BGP4MP_MESSAGE_AS4_ADDPATH           | 9     |
| BGP4MP_MESSAGE_LOCAL_ADDPATH         | 10    |
| BGP4MP_MESSAGE_AS4_LOCAL_ADDPATH     | 11    |

### BGP Message Types

Defined [here](https://www.iana.org/assignments/bgp-parameters/bgp-parameters.xml#bgp-parameters-1)

| Name              | Value |
|-------------------|-------|
| OPEN              | 1     |
| UPDATE            | 2     |
| NOTIFICATION      | 3     |
| KEEPALIVE         | 4     |
| ROUTE_REFRESH     | 5     |

### BGP Path Attributes

Defined [here](https://www.iana.org/assignments/bgp-parameters/bgp-parameters.xhtml#bgp-parameters-2)

| Name                     | Value |
|--------------------------|-------|
| ORIGIN                   | 1     |
| AS_PATH                  | 2     |
| NEXT_HOP                 | 3     |
| MULTI_EXIT_DISC          | 4     |
| LOCAL_PREF               | 5     |
| ATOMIC_AGGREGATE         | 6     |
| AGGREGATOR               | 7     |
| COMMUNITY                | 8     |
| ORIGINATOR_ID            | 9     |
| CLUSTER_LIST             | 10    |
| MP_REACH_NLRI            | 14    |
| MP_UNREACH_NLRI          | 15    |
| EXTENDED_COMMUNITIES     | 16    |
| AS4_PATH                 | 17    |
| AS4_AGGREGATOR           | 18    |
| AS_PATHLIMIT             | 21    |
| LARGE_COMMUNITY          | 32    |
| ATTR_SET                 | 128   |

### Address Family Identifiers (AFI) Values

Defined [here](https://www.iana.org/assignments/address-family-numbers/address-family-numbers.xhtml#address-family-numbers-2)

| Name | Value |
|------|-------|
| IPv4 | 1     |
| IPv6 | 2     |

### Subsequent Address Family Identifiers (SAFI) Values

Defined [here](https://www.iana.org/assignments/safi-namespace/safi-namespace.xhtml#safi-namespace-2)

| Name                             | Value |
|----------------------------------|-------|
| NLRI used for unicast forwarding | 1     |
| ~~BGP EVPNs~~                    | 70    |

### BGP Capabilities

Defined [here](https://www.iana.org/assignments/capability-codes/capability-codes.xhtml)

| Name                                          | Value |
|-----------------------------------------------|-------|
| Multiprotocol Extensions for BGP-4            | 1     |
| Route Refresh Capability for BGP-4            | 2     |
| Graceful Restart Capability                   | 64    |
| Enhanced Route Refresh Capability             | 70    |
| Long-Lived Graceful Restart (LLGR) Capability | 71    |

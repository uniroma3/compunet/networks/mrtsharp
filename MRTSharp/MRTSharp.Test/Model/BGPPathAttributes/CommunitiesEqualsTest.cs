﻿using MRTSharp.Model.BGP.PathAttributes;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xunit;

namespace MRTSharp.Test.Model.BGPPathAttributes
{
	public class CommunitiesEqualsTest
	{
		[Fact]
		public void TestExtendedSameReference()
		{
			BGPExtendedCommunity a = new()
			{
				Type = 12,
				SubType = 12
			};

			Assert.NotNull(a);

			Assert.Equal(a, a);
			Assert.True(a.Equals(a));
		}

		[Fact]
		public void TestExtendedDifferentReference()
		{
			BGPExtendedCommunity a = new()
			{
				Type = 12,
				SubType = 12
			};

			BGPExtendedCommunity b = new()
			{
				Type = 12,
				SubType = 12
			};

			Assert.Equal(a, b);
			Assert.Equal(a.GetHashCode(), b.GetHashCode());
			Assert.True(a == b);
			Assert.False(a != b);
		}

		[Fact]
		public void TestExtendedDifferentSubType()
		{
			BGPExtendedCommunity a = new()
			{
				Type = 12,
				SubType = 13
			};

			BGPExtendedCommunity b = new()
			{
				Type = 12,
				SubType = 12
			};

			Assert.NotEqual(a, b);
			Assert.NotEqual(a.GetHashCode(), b.GetHashCode());
			Assert.True(a != b);
			Assert.False(a == b);
		}

		[Fact]
		public void TestExtendedDifferentType()
		{
			BGPExtendedCommunity a = new()
			{
				Type = 15,
				SubType = 12
			};

			BGPExtendedCommunity b = new()
			{
				Type = 12,
				SubType = 12
			};

			Assert.NotEqual(a, b);
			Assert.NotEqual(a.GetHashCode(), b.GetHashCode());
			Assert.True(a != b);
			Assert.False(a == b);
		}

		[Fact]
		public void TestExtendedDifferentValue1()
		{
			BGPExtendedCommunity a = new()
			{
				Type = 12,
				SubType = 12,
				Value = Array.Empty<byte>()
			};

			BGPExtendedCommunity b = new()
			{
				Type = 12,
				SubType = 12
			};

			Assert.NotEqual(a, b);
			Assert.NotEqual(b, a);
			Assert.NotEqual(a.GetHashCode(), b.GetHashCode());
			Assert.True(a != b);
			Assert.False(a == b);
			Assert.True(b != a);
			Assert.False(b == a);
		}

		[Fact]
		public void TestExtendedDifferentValue2()
		{
			BGPExtendedCommunity a = new()
			{
				Type = 12,
				SubType = 12,
				Value = Array.Empty<byte>()
			};

			BGPExtendedCommunity b = new()
			{
				Type = 12,
				SubType = 12,
				Value = new byte[1]
			};

			Assert.NotEqual(a, b);
			Assert.NotEqual(b, a);
			Assert.NotEqual(a.GetHashCode(), b.GetHashCode());
			Assert.True(a != b);
			Assert.False(a == b);
			Assert.True(b != a);
			Assert.False(b == a);
		}

		[Fact]
		public void TestExtendedDifferentValue3()
		{
			BGPExtendedCommunity a = new()
			{
				Type = 12,
				SubType = 12,
				Value = Array.Empty<byte>()
			};

			BGPExtendedCommunity b = new()
			{
				Type = 12,
				SubType = 12,
				Value = new byte[1] {1}
			};

			Assert.NotEqual(a, b);
			Assert.NotEqual(b, a);
			Assert.NotEqual(a.GetHashCode(), b.GetHashCode());
			Assert.True(a != b);
			Assert.False(a == b);
			Assert.True(b != a);
			Assert.False(b == a);
		}

		[Fact]
		public void TestExtendedDifferentValue4()
		{
			BGPExtendedCommunity a = new()
			{
				Type = 12,
				SubType = 12,
				Value = new byte[1] { 2 }
			};

			BGPExtendedCommunity b = new()
			{
				Type = 12,
				SubType = 12,
				Value = new byte[1] { 1 }
			};

			Assert.NotEqual(a, b);
			Assert.NotEqual(b, a);
			Assert.NotEqual(a.GetHashCode(), b.GetHashCode());
			Assert.True(a != b);
			Assert.False(a == b);
			Assert.True(b != a);
			Assert.False(b == a);
		}

		[Fact]
		public void TestExtendedEqualsNull()
		{
			BGPExtendedCommunity a = new()
			{
				Type = 12,
				SubType = 13
			};

			Assert.False(a.Equals(null));
			Assert.True(a != null);
			Assert.False(a == null);
		}

		[Fact]
		public void TestExtendedDifferentGenericObject()
		{
			BGPExtendedCommunity a = new()
			{
				Type = 12,
				SubType = 12
			};

			Object b = new BGPExtendedCommunity()
			{
				Type = 12,
				SubType = 12
			};

			Assert.NotNull(a);
			Assert.NotNull(b);

			Assert.Equal(a, b);
			Assert.True(a.Equals(b));
			Assert.Equal(a.GetHashCode(), b.GetHashCode());
		}
	}
}

﻿using MRTSharp.Model.BGP;
using MRTSharp.Model.IP;
using System.Collections.Generic;
using Xunit;

namespace MRTSharp.Test.Model.BGP4MPUpdateTest
{
	public class ContainsWithdrawalsTest
	{
		[Fact]
		public void TestNullWithdrawals()
		{
			BGPMessageUpdate update = new();
			Assert.False(update.ContainsWithdrawals());
		}

		[Fact]
		public void TestOneWithdrawal()
		{
			BGPMessageUpdate update = new()
			{
				Withdrawals = new List<IPPrefix>()
			};
			update.Withdrawals.Add(IPPrefix.Parse("192.168.0.0/16"));
			Assert.True(update.ContainsWithdrawals());
		}

	}
}

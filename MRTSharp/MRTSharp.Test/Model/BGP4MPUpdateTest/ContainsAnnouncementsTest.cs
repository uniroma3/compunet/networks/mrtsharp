﻿using MRTSharp.Model.BGP;
using MRTSharp.Model.IP;
using System.Collections.Generic;
using Xunit;

namespace MRTSharp.Test.Model.BGP4MPUpdateTest
{
	public class ContainsAnnouncementsTest
	{
		[Fact]
		public void TestNullWithdrawals()
		{
			BGPMessageUpdate update = new();
			Assert.False(update.ContainsAnnouncements());
		}

		[Fact]
		public void TestOneWithdrawal()
		{
			BGPMessageUpdate update = new()
			{
				Announcements = new List<IPPrefix>()
			};
			update.Announcements.Add(IPPrefix.Parse("192.168.0.0/16"));
			Assert.True(update.ContainsAnnouncements());
		}
	}
}

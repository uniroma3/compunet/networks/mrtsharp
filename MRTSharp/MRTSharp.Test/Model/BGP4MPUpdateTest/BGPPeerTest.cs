﻿using MRTSharp.Model.BGP4MP.Peer.Raw;
using Xunit;

namespace MRTSharp.Test.Model.BGP4MPUpdateTest
{
	public class BGPPeerTest
	{
		[Fact]
		public void TestEquals4Bytes()
		{
			BGPPeer4 peer = new();
			peer.SetPeerAS(12);

			BGPPeer4 peer2 = new();
			peer2.SetPeerAS(12);
			Assert.Equal(peer, peer2);
		}

		[Fact]
		public void TestEquals2Bytes()
		{
			BGPPeer2 peer = new();
			peer.SetPeerAS(12);

			BGPPeer2 peer2 = new();
			peer2.SetPeerAS(12);
			Assert.Equal(peer, peer2);
		}

		[Fact]
		public void TestEquals2BytesViaInterface()
		{
			BGPPeer2 peer = new();
			peer.SetPeerAS(12);
			IBGPPeer iPeer = peer;

			BGPPeer2 peer2 = new();
			peer2.SetPeerAS(12);
			IBGPPeer iPeer2 = peer2;

			Assert.Equal(iPeer, iPeer2);
		}

		[Fact]
		public void TestEquals4BytesViaInterface()
		{
			BGPPeer4 peer = new();
			peer.SetPeerAS(12);
			IBGPPeer iPeer = peer;

			BGPPeer4 peer2 = new();
			peer2.SetPeerAS(12);
			IBGPPeer iPeer2 = peer2;

			Assert.Equal(iPeer, iPeer2);
		}

		[Fact]
		public void TestEqualsInteroperabilityViaInterface()
		{
			BGPPeer4 peer = new();
			peer.SetPeerAS(12);
			IBGPPeer iPeer = peer;

			BGPPeer2 peer2 = new();
			peer2.SetPeerAS(12);
			IBGPPeer iPeer2 = peer2;

			Assert.Equal(iPeer, iPeer2);
		}

		[Fact]
		public void TestEqualsInteroperabilityViaInterface2()
		{
			BGPPeer4 peer = new();
			peer.SetPeerAS(12);
			IBGPPeer iPeer = peer;

			BGPPeer2 peer2 = new();
			peer2.SetPeerAS(12);
			IBGPPeer iPeer2 = peer2;

			Assert.Equal(iPeer2, iPeer);
		}
	}
}

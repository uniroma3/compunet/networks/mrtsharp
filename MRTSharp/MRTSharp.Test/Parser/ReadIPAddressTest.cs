﻿using MRTSharp.File;
using MRTSharp.Model.IP;
using MRTSharp.Utils;
using Xunit;

namespace MRTSharp.Test.Parser
{
	public class ReadIPAddressTest
	{
		[Fact]
		public void TestDefaultNetworkv4()
		{
			FileBuffer buffer = FileBuffer.FromBytes(new byte[] { 0, 0, 0, 0 });
			Assert.Equal("0.0.0.0", IPUtils.ReadIPAddress(buffer, IPType.IPv4).ToString());
		}

		[Fact]
		public void TestIPv4IP()
		{
			FileBuffer buffer = FileBuffer.FromBytes(new byte[] { 192, 168, 0, 1 });
			Assert.Equal("192.168.0.1", IPUtils.ReadIPAddress(buffer, IPType.IPv4).ToString());
		}

		[Fact]
		public void TestIPv4IPNotFromBeginning()
		{
			FileBuffer buffer = FileBuffer.FromBytes(new byte[] { 0, 12, 192, 168, 0, 1 });
			buffer.Seek(2);
			Assert.Equal("192.168.0.1", IPUtils.ReadIPAddress(buffer, IPType.IPv4).ToString());
		}

		[Fact]
		public void TestIPv4IPTwoNotFromBeginning()
		{
			FileBuffer buffer = FileBuffer.FromBytes(new byte[] { 0, 12, 192, 168, 0, 1, 192, 168, 0, 2 });
			buffer.Seek(2);
			Assert.Equal("192.168.0.1", IPUtils.ReadIPAddress(buffer, IPType.IPv4).ToString());
			Assert.Equal("192.168.0.2", IPUtils.ReadIPAddress(buffer, IPType.IPv4).ToString());
		}

		[Fact]
		public void TestIPv6IP()
		{
			FileBuffer buffer = FileBuffer.FromBytes(new byte[] { 42, 9, 4, 192, 0, 1, 139, 28, 0, 0, 0, 0, 0, 0, 99, 99 });
			Assert.Equal("2a09:4c0:1:8b1c::6363", IPUtils.ReadIPAddress(buffer, IPType.IPv6).ToString());
		}

		[Fact]
		public void TestIPv6IP2()
		{
			FileBuffer buffer = FileBuffer.FromBytes(new byte[] { 32, 1, 6, 124, 2, 232, 0, 2, 255, 255, 0, 0, 0, 4, 0, 40 });
			Assert.Equal("2001:67c:2e8:2:ffff:0:4:28", IPUtils.ReadIPAddress(buffer, IPType.IPv6).ToString());
		}

		[Fact]
		public void TestIPv6IP3()
		{
			FileBuffer buffer = FileBuffer.FromBytes(new byte[] { 42, 2, 71, 160, 0, 10, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1 });
			Assert.Equal("2a02:47a0:a::1", IPUtils.ReadIPAddress(buffer, IPType.IPv6).ToString());
		}

		[Fact]
		public void TestIPv6IP4()
		{
			FileBuffer buffer = FileBuffer.FromBytes(new byte[] { 42, 9, 75, 199, 208, 33, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0 });
			Assert.Equal("2a09:4bc7:d021::", IPUtils.ReadIPAddress(buffer, IPType.IPv6).ToString());
		}
	}
}

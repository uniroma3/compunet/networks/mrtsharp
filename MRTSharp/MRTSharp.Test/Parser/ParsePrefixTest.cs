﻿using MRTSharp.Model.IP;
using System;
using Xunit;

namespace MRTSharp.Test.Parser
{
	public class ParsePrefixTest
	{
		[Fact]
		public void TestDefaultNetworkv4()
		{
			String prefix = "0.0.0.0/0";
			Assert.Equal(prefix, IPPrefix.Parse(prefix).ToString());
		}

		[Fact]
		public void TestDefaultNetworkv6()
		{
			String prefix = "::/0";
			Assert.Equal(prefix, IPPrefix.Parse(prefix).ToString());
		}

		[Fact]
		public void TestNetworkv4()
		{
			String prefix = "192.168.0.0/16";
			Assert.Equal(prefix, IPPrefix.Parse(prefix).ToString());
		}

		[Fact]
		public void TestNetworkv4Dirty()
		{
			Assert.Equal("192.168.0.0/16", IPPrefix.Parse("192.168.128.0/16").ToString());
		}

		[Fact]
		public void TestNetworkv4DirtyHalfByte()
		{
			Assert.Equal("192.168.0.0/17", IPPrefix.Parse("192.168.23.0/17").ToString());
		}

		[Fact]
		public void TestNetworkv4DirtySecondHalfByte()
		{
			Assert.Equal("192.168.128.0/17", IPPrefix.Parse("192.168.153.0/17").ToString());
		}

		[Fact]
		public void TestNetworkv6()
		{
			String prefix = "2a03:2880:f11c:8183:face:b00c:0:25de/128";
			Assert.Equal(prefix, IPPrefix.Parse(prefix).ToString());
		}

		#region Negative Tests
		[Fact]
		public void TestWrongNetworkv6()
		{
			Assert.NotEqual("2a04:2880:f11c:8183:face:b00c:0:25de/128", IPPrefix.Parse("2a03:2880:f11c:8183:face:b00c:0:25de/128").ToString());
		}

		[Fact]
		public void TestWrongNetworkv4()
		{
			Assert.NotEqual("192.168.0.0/16", IPPrefix.Parse("193.168.0.0/16").ToString());
		}

		[Fact]
		public void TestWrongCIDRNetworkv4()
		{
			Assert.NotEqual("192.168.0.0/15", IPPrefix.Parse("192.168.0.0/16").ToString());
		}

		[Fact]
		public void TestWrongCIDRNetworkv6()
		{
			Assert.NotEqual("2a03:2880:f11c:8183:face:b00c:0:25de/120", IPPrefix.Parse("2a03:2880:f11c:8183:face:b00c:0:25de/128").ToString());
		}
		#endregion

		#region Exceptions
		[Fact]
		public void TestEmptyString()
		{
			Exception ex = Assert.Throws<ArgumentNullException>(() => IPPrefix.Parse(""));
			Assert.Equal("Value cannot be null. (Parameter 'stringPrefix')", ex.Message);
		}

		[Fact]
		public void TestNullString()
		{
			Exception ex = Assert.Throws<ArgumentNullException>(() => IPPrefix.Parse(null));
			Assert.Equal("Value cannot be null. (Parameter 'stringPrefix')", ex.Message);
		}

		[Fact]
		public void TestNullStringWithCIDR()
		{
			Exception ex = Assert.Throws<ArgumentNullException>(() => IPPrefix.Parse(null, 12));
			Assert.Equal("Value cannot be null. (Parameter 'ipAddress')", ex.Message);
		}

		[Fact]
		public void TestStringWithoutSlash()
		{
			Exception ex = Assert.Throws<FormatException>(() => IPPrefix.Parse("192.168.0.1"));
			Assert.Equal("CIDR not defined.", ex.Message);
		}

		[Fact]
		public void TestTextString()
		{
			Exception ex = Assert.Throws<FormatException>(() => IPPrefix.Parse("HelloWorld"));
			Assert.Equal("CIDR not defined.", ex.Message);
		}

		[Fact]
		public void TestTextStringWithSlashString()
		{
			Exception ex = Assert.Throws<FormatException>(() => IPPrefix.Parse("HelloWorld/Hello"));
			Assert.Equal("Input string was not in a correct format.", ex.Message);
		}

		[Fact]
		public void TestTextStringWithSlashNumeric()
		{
			Exception ex = Assert.Throws<FormatException>(() => IPPrefix.Parse("HelloWorld/12"));
			Assert.Equal("An invalid IP address was specified.", ex.Message);
		}

		[Fact]
		public void TestNetworkv4CIDRTooLarge()
		{
			Exception ex = Assert.Throws<FormatException>(() => IPPrefix.Parse("192.168.0.2/35"));
			Assert.Equal("CIDR not valid.", ex.Message);
		}

		[Fact]
		public void TestNetworkv6CIDRTooLarge()
		{
			Exception ex = Assert.Throws<FormatException>(() => IPPrefix.Parse("2a03:2880:f11c:8183:face:b00c:0:25de/130"));
			Assert.Equal("CIDR not valid.", ex.Message);
		}

		[Fact]
		public void TestNetworkv4CIDRNegative()
		{
			Exception ex = Assert.Throws<FormatException>(() => IPPrefix.Parse("192.168.0.2/-35"));
			Assert.Equal("CIDR not valid.", ex.Message);
		}

		[Fact]
		public void TestNetworkv6CIDRNegative()
		{
			Exception ex = Assert.Throws<FormatException>(() => IPPrefix.Parse("2a03:2880:f11c:8183:face:b00c:0:25de/-130"));
			Assert.Equal("CIDR not valid.", ex.Message);
		}
		#endregion
	}
}

using MRTSharp.File;
using MRTSharp.Model.IP;
using MRTSharp.Utils;
using System;
using Xunit;

namespace MRTSharp.Test.Parser
{
	public class ReadPrefixTest
	{
		#region IPv4
		[Fact]
		public void TestDefaultNetworkv4()
		{
			FileBuffer buffer = FileBuffer.FromBytes(new byte[] { 0 });

			Assert.Equal(IPPrefix.Parse("0.0.0.0/0"), IPUtils.ReadPrefix(buffer, IPType.IPv4));
		}

		[Fact]
		public void TestSimplePrivateNetworkv4()
		{
			FileBuffer buffer = FileBuffer.FromBytes(new byte[] { 22, 45, 231, 8 });

			Assert.Equal(IPPrefix.Parse("45.231.8.0/22"), IPUtils.ReadPrefix(buffer, IPType.IPv4));
		}

		[Fact]
		public void TestSimpleNetworkv4()
		{
			FileBuffer buffer = FileBuffer.FromBytes(new byte[] { 18, 186, 248, 64 });

			Assert.Equal(IPPrefix.Parse("186.248.64.0/18"), IPUtils.ReadPrefix(buffer, IPType.IPv4));
		}

		[Fact]
		public void TestCutNetworkv4()
		{
			FileBuffer buffer = FileBuffer.FromBytes(new byte[] { 18, 186, 248, 68 });

			Assert.Equal(IPPrefix.Parse("186.248.64.0/18"), IPUtils.ReadPrefix(buffer, IPType.IPv4));
		}
		#endregion

		#region IPv6
		[Fact]
		public void TestDefaultNetworkv6()
		{
			FileBuffer buffer = FileBuffer.FromBytes(new byte[] { 0 });

			Assert.Equal(IPPrefix.Parse("::/0"), IPUtils.ReadPrefix(buffer, IPType.IPv6));
		}

		[Fact]
		public void TestSimpleNetwork1v6()
		{
			FileBuffer buffer = FileBuffer.FromBytes(new byte[] { 48, 32, 1, 13, 240, 0, 182 });

			Assert.Equal(IPPrefix.Parse("2001:df0:b6::/48"), IPUtils.ReadPrefix(buffer, IPType.IPv6));
		}

		[Fact]
		public void TestSimpleNetwork2v6()
		{
			FileBuffer buffer = FileBuffer.FromBytes(new byte[] { 33, 40, 4, 67, 48, 128 });

			Assert.Equal(IPPrefix.Parse("2804:4330:8000::/33"), IPUtils.ReadPrefix(buffer, IPType.IPv6));
		}

		[Fact]
		public void TestSimpleNetwork3v6()
		{
			FileBuffer buffer = FileBuffer.FromBytes(new byte[] { 48, 32, 1, 7, 251, 255, 2 });

			Assert.Equal(IPPrefix.Parse("2001:7fb:ff02::/48"), IPUtils.ReadPrefix(buffer, IPType.IPv6));
		}
		#endregion

		#region Exceptions
		[Fact]
		public void TestExceptionIPTypeDefault()
		{
			FileBuffer buffer = FileBuffer.FromBytes(new byte[] { 0 });

			Exception ex = Assert.Throws<Exception>(() => IPUtils.ReadPrefix(buffer, IPType.Null));

			Assert.Equal("Error in IPType", ex.Message);
		}

		[Fact]
		public void TestExceptionIPTypeNotDefault()
		{
			FileBuffer buffer = FileBuffer.FromBytes(new byte[] { 1, 7 });

			Exception ex = Assert.Throws<Exception>(() => IPUtils.ReadPrefix(buffer, IPType.Null));

			Assert.Equal("Error in IPType", ex.Message);
		}
		#endregion
	}
}


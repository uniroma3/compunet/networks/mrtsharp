using MRTSharp.File;
using MRTSharp.Model.BGP;
using MRTSharp.Model.BGP.PathAttributes;
using MRTSharp.Tasks.BGP.Update.PathAttributes;
using MRTSharp.Utils;
using System;
using System.Collections.Generic;
using Xunit;

namespace MRTSharp.Test.Parser
{
	public class ReadCommunitiesTest
	{
		[Fact]
		public void TestCommunities1()
		{
			UInt16 attributeLength = 8;
			FileBuffer buffer = FileBuffer.FromBytes(new byte[] {
				0, 0, 154, 109,
				154, 109, 194, 2
			});

			BGPMessageUpdate rightUpdate = new()
			{
				Communities = new List<IBGPCommunity>()
				{
					new BGPCommunity(0, 39533),
					new BGPCommunity(39533, 49666)
				}
			};

			BGPMessageUpdate parsedUpdate = new();

			UInt16 flags = FlagUtils.GetFlags(2, false, false);
			Singleton<BGPCommunityAttributeTask>.Instance.Run(buffer, ref parsedUpdate, attributeLength, flags);

			Assert.Equal(rightUpdate, parsedUpdate);
		}

		[Fact]
		public void TestCommunitiesAlreadyRead()
		{
			UInt16 attributeLength = 8;
			FileBuffer buffer = FileBuffer.FromBytes(new byte[] {
				0, 0, 154, 109,
				154, 109, 194, 2
			});

			BGPMessageUpdate rightUpdate = new()
			{
				Communities = new List<IBGPCommunity>()
				{
					new BGPCommunity(0, 39533),
					new BGPCommunity(39533, 49666)
				}
			};

			BGPMessageUpdate parsedUpdate = new()
			{
				Communities = new List<IBGPCommunity>()
			};

			UInt16 flags = FlagUtils.GetFlags(2, false, false);
			Singleton<BGPCommunityAttributeTask>.Instance.Run(buffer, ref parsedUpdate, attributeLength, flags);

			Assert.Equal(rightUpdate, parsedUpdate);
		}

		[Fact]
		public void TestCommunitiesEmpty()
		{
			UInt16 attributeLength = 0;
			FileBuffer buffer = FileBuffer.FromBytes(Array.Empty<byte>());

			BGPMessageUpdate rightUpdate = new()
			{
				Communities = new List<IBGPCommunity>()
			};

			BGPMessageUpdate parsedUpdate = new()
			{
				Communities = new List<IBGPCommunity>()
			};

			UInt16 flags = FlagUtils.GetFlags(2, false, false);
			Singleton<BGPCommunityAttributeTask>.Instance.Run(buffer, ref parsedUpdate, attributeLength, flags);

			Assert.Equal(rightUpdate, parsedUpdate);
		}

		#region Exceptions
		[Fact]
		public void TestAttributeLengthStrange()
		{
			UInt16 attributeLength = 9;

			FileBuffer buffer = FileBuffer.FromBytes(new byte[] {
				0, 0, 154, 109,
				154, 109, 194, 2,
				37
			});

			BGPMessageUpdate parsedUpdate = new();

			UInt16 flags = FlagUtils.GetFlags(2, false, false);
			Exception ex = Assert.Throws<Exception>(() => Singleton<BGPCommunityAttributeTask>.Instance.Run(buffer, ref parsedUpdate, attributeLength, flags));

			Assert.Equal("Community field length malformed", ex.Message);
		}
		#endregion
	}
}

using MRTSharp.File;
using MRTSharp.Model.BGP;
using MRTSharp.Model.BGP.PathAttributes;
using MRTSharp.Tasks.BGP.Update.PathAttributes;
using MRTSharp.Utils;
using System;
using System.Collections.Generic;
using Xunit;

namespace MRTSharp.Test.Parser
{
	public class ReadASPathTest
	{
		[Fact]
		public void TestAsPath4Byte()
		{
			UInt16 attributeLength = 34;

			FileBuffer buffer = FileBuffer.FromBytes(new byte[] {
				2, 8,
				0, 0, 154, 109,
				0, 0, 27, 27,
				0, 0, 13, 28,
				0, 0, 65, 95,
				0, 0, 65, 95,
				0, 0, 65, 95,
				0, 4, 2, 104,
				0, 4, 0, 219
			});

			BGPMessageUpdate rightUpdate = new()
			{
				ASPath = new List<ASEntry>
				{
					new ASEntry
					{
						ASNumbers = new UInt32[]{39533, 6939, 3356, 16735, 16735, 16735, 262760, 262363},
						SegmentType = ASPathSegmentType.AS_SEQUENCE
					}
				}
			};

			BGPMessageUpdate parsedUpdate = new();

			UInt16 flags = FlagUtils.GetFlags(4, false, false);
			Singleton<BGPASPathAttributeTask>.Instance.Run(buffer, ref parsedUpdate, attributeLength, flags);

			Assert.Equal(rightUpdate, parsedUpdate);
		}

		[Fact]
		public void TestAsPath2Byte()
		{
			UInt16 attributeLength = 14;

			FileBuffer buffer = FileBuffer.FromBytes(new byte[] {
				2, 6,
				154, 109,
				27, 27,
				13, 28,
				65, 95,
				65, 95,
				65, 95,
			});

			BGPMessageUpdate rightUpdate = new()
			{
				ASPath = new List<ASEntry>
				{
					new ASEntry
					{
						 ASNumbers = new UInt32[]{39533, 6939, 3356, 16735, 16735, 16735},
						 SegmentType = ASPathSegmentType.AS_SEQUENCE
					}
				}
			};

			BGPMessageUpdate parsedUpdate = new();

			UInt16 flags = FlagUtils.GetFlags(2, false, false);
			Singleton<BGPASPathAttributeTask>.Instance.Run(buffer, ref parsedUpdate, attributeLength, flags);

			Assert.Equal(rightUpdate, parsedUpdate);
		}

		[Fact]
		public void TestAsSet4Byte()
		{
			UInt16 attributeLength = 34;

			FileBuffer buffer = FileBuffer.FromBytes(new byte[] {
				1, 8,
				0, 0, 154, 109,
				0, 0, 27, 27,
				0, 0, 13, 28,
				0, 0, 65, 95,
				0, 0, 65, 95,
				0, 0, 65, 95,
				0, 4, 2, 104,
				0, 4, 0, 219
			});

			BGPMessageUpdate rightUpdate = new()
			{
				ASPath = new List<ASEntry>
				{
					new ASEntry
					{
						ASNumbers = new UInt32[]{39533, 6939, 3356, 16735, 16735, 16735, 262760, 262363},
						SegmentType = ASPathSegmentType.AS_SET
					}
				}
			};

			BGPMessageUpdate parsedUpdate = new();

			UInt16 flags = FlagUtils.GetFlags(4, false, false);
			Singleton<BGPASPathAttributeTask>.Instance.Run(buffer, ref parsedUpdate, attributeLength, flags);

			Assert.Equal(rightUpdate, parsedUpdate);
		}

		[Fact]
		public void TestAsSet2Byte()
		{
			UInt16 attributeLength = 14;

			FileBuffer buffer = FileBuffer.FromBytes(new byte[] {
				1, 6,
				154, 109,
				27, 27,
				13, 28,
				65, 95,
				65, 95,
				65, 95,
			});

			BGPMessageUpdate rightUpdate = new()
			{
				ASPath = new List<ASEntry>
				{
					new ASEntry {
						ASNumbers = new UInt32[]{39533, 6939, 3356, 16735, 16735, 16735},
						SegmentType = ASPathSegmentType.AS_SET
					}
				}
			};

			BGPMessageUpdate parsedUpdate = new();

			UInt16 flags = FlagUtils.GetFlags(2, false, false);
			Singleton<BGPASPathAttributeTask>.Instance.Run(buffer, ref parsedUpdate, attributeLength, flags);

			Assert.Equal(rightUpdate, parsedUpdate);
		}

		#region Exceptions
		[Fact]
		public void TestPathSegmentTypeWrong()
		{
			UInt16 attributeLength = 14;

			FileBuffer buffer = FileBuffer.FromBytes(new byte[] {
				5, 6,
				154, 109,
				27, 27,
				13, 28,
				65, 95,
				65, 95,
				65, 95,
			});

			BGPMessageUpdate rightUpdate = new()
			{
				ASPath = new List<ASEntry>(),
				Malformed = true
			};


			BGPMessageUpdate parsedUpdate = new();

			UInt16 flags = FlagUtils.GetFlags(2, false, false);
			Singleton<BGPASPathAttributeTask>.Instance.Run(buffer, ref parsedUpdate, attributeLength, flags);

			Assert.Equal(rightUpdate, parsedUpdate);
		}

		[Fact]
		public void TestPathSegmentTypeWrongHalfArray()
		{
			UInt16 attributeLength = 16;

			FileBuffer buffer = FileBuffer.FromBytes(new byte[] {
				2, 3,
				154, 109,
				27, 27,
				13, 28,
				5, 3,
				65, 95,
				65, 95,
				65, 95,
			});

			BGPMessageUpdate rightUpdate = new()
			{
				ASPath = new List<ASEntry>
				{
					new ASEntry
					{
						ASNumbers = new UInt32[]{39533, 6939, 3356},
						SegmentType = ASPathSegmentType.AS_SEQUENCE
					}
				},
				Malformed = true
			};

			BGPMessageUpdate parsedUpdate = new();

			UInt16 flags = FlagUtils.GetFlags(2, false, false);
			Singleton<BGPASPathAttributeTask>.Instance.Run(buffer, ref parsedUpdate, attributeLength, flags);

			Assert.Equal(rightUpdate, parsedUpdate);
		}

		[Fact]
		public void TestPathSegmentLengthAndTypeWrong()
		{
			UInt16 attributeLength = 16;

			FileBuffer buffer = FileBuffer.FromBytes(new byte[] {
				2, 3,
				154, 109,
				27, 27,
				13, 28,
				5, 3,	// Wrong PathSegmentType = 5
				65, 95,
				65, 95,
				65, 95,
			});

			BGPMessageUpdate rightUpdate = new()
			{
				ASPath = new List<ASEntry>
				{
					new ASEntry
					{
						ASNumbers = new UInt32[]{39533, 6939, 3356},
						SegmentType = ASPathSegmentType.AS_SEQUENCE
					}
				},
				Malformed = true
			};

			BGPMessageUpdate parsedUpdate = new();

			UInt16 flags = FlagUtils.GetFlags(2, false, false);
			Singleton<BGPASPathAttributeTask>.Instance.Run(buffer, ref parsedUpdate, attributeLength, flags);

			Assert.Equal(rightUpdate, parsedUpdate);
		}

		[Fact]
		public void TestPathSegmentAsEntryLengthWrong()
		{
			UInt16 attributeLength = 16;

			FileBuffer buffer = FileBuffer.FromBytes(new byte[] {
				2, 3,
				154, 109,
				27, 27,
				13, 28,
				2, 10,
				65, 95,
				65, 95,
				65, 95,
			});

			BGPMessageUpdate rightUpdate = new()
			{
				ASPath = new List<ASEntry>
				{
					new ASEntry
					{
						ASNumbers = new UInt32[]{39533, 6939, 3356},
						SegmentType = ASPathSegmentType.AS_SEQUENCE
					}
				},
				Malformed = true
			};

			BGPMessageUpdate parsedUpdate = new();

			UInt16 flags = FlagUtils.GetFlags(2, false, false);
			Singleton<BGPASPathAttributeTask>.Instance.Run(buffer, ref parsedUpdate, attributeLength, flags);

			Assert.Equal(rightUpdate, parsedUpdate);
		}

		[Fact]
		public void TestPathSegmentAttributeLengthWrong()
		{
			UInt16 attributeLength = 16;

			FileBuffer buffer = FileBuffer.FromBytes(new byte[] {
				2, 15,
				154, 109,
				27, 27,
				13, 28,
				2, 10,
				65, 95,
				65, 95,
				65, 95,
			});

			BGPMessageUpdate rightUpdate = new()
			{
				ASPath = new List<ASEntry>(),
				Malformed = true
			};

			BGPMessageUpdate parsedUpdate = new();

			UInt16 flags = FlagUtils.GetFlags(2, false, false);
			Singleton<BGPASPathAttributeTask>.Instance.Run(buffer, ref parsedUpdate, attributeLength, flags);

			Assert.Equal(rightUpdate, parsedUpdate);
		}
		#endregion
	}
}

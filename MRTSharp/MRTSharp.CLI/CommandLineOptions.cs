﻿using CommandLine;
using MRTSharp.CLI.Model;
using System;
using System.Collections.Generic;
using System.Linq;

namespace MRTSharp.CLI
{
	public class CommandLineOptions
	{
		[Option('p', "path", Required = true, HelpText = "Path of the MRT Files, can be files or folders.")]
		public IEnumerable<String> Path { get; set; }

		[Option("socket", HelpText = "Path to the output socket file.", SetName = "Output socket file", Required = true)]
		public String Socket { get; set; }

		[Option("stdout", HelpText = "Output to StdOut.", SetName = "Output StdOut", Required = true)]
		public bool StdOut { get; set; }

		[Option("file", HelpText = "Path to the output text file.", SetName = "Output file path", Required = true)]
		public String OutputFile { get; set; }

		[Option('f', "format", Required = true, HelpText = "Output format. Can be json or tsv.")]
		public Format Format { get; set; }

		[Option('t', "multithread", HelpText = "Enable multithreaded execution. It works only if multiple files are provided")]
		public bool Multithreaded { get; set; }

		public void FormatPaths()
		{
			Path = Path.Select(x => System.IO.Path.GetFullPath(x));
		}

		public IEnumerable<String> GetInvalidPaths()
		{
			return Path.Where(x => !System.IO.File.Exists(x) && !System.IO.Directory.Exists(x));
		}
	}
}

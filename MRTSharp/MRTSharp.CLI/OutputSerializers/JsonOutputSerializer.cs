﻿using MRTSharp.CLI.Converters;
using MRTSharp.Model.BGP;
using MRTSharp.Model.BGP4MP;
using System.Text.Json;
using System.Text.Json.Serialization;

namespace MRTSharp.CLI.OutputSerializers
{
	public class JsonOutputSerializer : IOutputSerializer
	{
		private readonly JsonSerializerOptions _options = new()
		{
			IncludeFields = true,
			WriteIndented = false,
			DefaultIgnoreCondition = JsonIgnoreCondition.WhenWritingNull,
			Converters =
						{
							new JsonStringEnumConverter(),
							new IPAddressConverter(),
							new IPPrefixConverter(),
							new IBGPCommunityConverter()
						}
		};
		public byte[] Serialize(BGP4MPMessage entry, BGPMessageUpdate data)
		{
			return JsonSerializer.SerializeToUtf8Bytes(data, _options);
		}
	}
}

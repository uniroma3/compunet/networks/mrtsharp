﻿using MRTSharp.Model.IP;
using System;
using System.Text.Json;
using System.Text.Json.Serialization;

namespace MRTSharp.CLI.Converters
{
	public class IPPrefixConverter : JsonConverter<IPPrefix>
	{
		public override IPPrefix Read(ref Utf8JsonReader reader, Type typeToConvert, JsonSerializerOptions options)
		{
			return IPPrefix.Parse(reader.GetString());
		}


		public override void Write(Utf8JsonWriter writer, IPPrefix iPAddress, JsonSerializerOptions options)
		{
			writer.WriteStringValue(iPAddress.ToString());
		}
	}
}

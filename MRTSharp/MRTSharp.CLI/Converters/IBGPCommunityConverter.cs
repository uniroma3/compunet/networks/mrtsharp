﻿using MRTSharp.Model.BGP.PathAttributes;
using System;
using System.Net;
using System.Text;
using System.Text.Json;
using System.Text.Json.Serialization;

namespace MRTSharp.CLI.Converters
{
	class IBGPCommunityConverter : JsonConverter<IBGPCommunity>
	{
		public override IBGPCommunity Read(ref Utf8JsonReader reader, Type typeToConvert, JsonSerializerOptions options)
		{
			throw new NotImplementedException();
		}


		public override void Write(Utf8JsonWriter writer, IBGPCommunity community, JsonSerializerOptions options)
		{
			var type = community.GetType();
			JsonSerializer.Serialize(writer, community, type, options);

			//writer.WriteStartArray();
			//writer.WriteNumberValue(community.ASNumber);
			//writer.WriteNumberValue(community.Value);
			//writer.WriteEndArray();
		}
	}
}

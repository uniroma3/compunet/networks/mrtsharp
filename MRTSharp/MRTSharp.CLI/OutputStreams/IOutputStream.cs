﻿using System;

namespace MRTSharp.CLI.OutputStreams
{
	public interface IOutputStream
	{
		public void Write(Byte[] line);
		public void SafeWrite(Byte[] line);
		public void Open();
		public void Close();
	}
}

﻿using System;
using System.Net.Sockets;

namespace MRTSharp.CLI.OutputStreams
{
	public class SocketOutputStream : IOutputStream
	{
		private readonly Socket _socket;
		private readonly String _socketPath;
		private readonly Object _lock = new();

		public SocketOutputStream(String socketPath)
		{
			_socket = new(AddressFamily.Unix, SocketType.Stream, ProtocolType.Unspecified);
			_socketPath = socketPath;
		}

		public void Close()
		{
			_socket.Close();
		}

		public void Open()
		{
			_socket.Connect(new UnixDomainSocketEndPoint(_socketPath));
		}

		public void SafeWrite(byte[] line)
		{
			lock (_lock)
			{
				_socket.Send(line);
			}
		}

		public void Write(byte[] line)
		{
			_socket.Send(line);
		}
	}
}

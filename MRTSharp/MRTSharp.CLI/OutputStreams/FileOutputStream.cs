﻿using System;

namespace MRTSharp.CLI.OutputStreams
{
	public class FileOutputStream : IOutputStream
	{
		private System.IO.FileStream _stream;
		private readonly String _fileName;
		private readonly Object _lock = new();

		public FileOutputStream(String fileName)
		{
			_fileName = fileName;
		}
		public void Close()
		{
			_stream.Close();
		}

		public void Open()
		{
			_stream = System.IO.File.Create(_fileName);
		}

		public void Write(Byte[] line)
		{
			_stream.Write(line);
		}

		public void SafeWrite(byte[] line)
		{
			lock (_lock)
			{
				_stream.Write(line);
			}
		}
	}
}

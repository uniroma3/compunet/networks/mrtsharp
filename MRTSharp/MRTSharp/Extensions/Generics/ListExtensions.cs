﻿using System.Collections.Generic;
using System.Linq;

namespace MRTSharp.Extensions.Generics
{
	public static class ListExtensions
	{
		public static List<uint> Clone(this List<uint> listToClone)
		{
			return new List<uint>(listToClone);
		}

		public static List<List<uint>> Clone(this List<List<uint>> listToClone)
		{
			return listToClone.Select(item => item.Clone()).ToList();
		}
	}
}

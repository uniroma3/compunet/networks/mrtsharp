using MRTSharp.Extensions.Generics;
using MRTSharp.Model.BGP;
using MRTSharp.Model.BGP.PathAttributes;
using System;
using System.Collections.Generic;
using System.Linq;

namespace MRTSharp.Extensions.BGP
{
	public static class ASPathExtensions
	{
		public static UInt32 GetASOrigin(this BGPMessageUpdate update)
		{
			if (update is null)
			{
				throw new ArgumentNullException(nameof(update));
			}

			if (update.ASPath is null)
			{
				throw new Exception("This update has no AS Path");
			}

			return update.ASPath[^1].ASNumbers[^1];
		}

		public static List<List<UInt32>> GetAllPossibleAsPaths(this BGPMessageUpdate update)
		{
			if (update is null)
			{
				throw new ArgumentNullException(nameof(update));
			}

			return update.ASPath.GetAllPossibleAsPaths();
		}

		public static List<List<UInt32>> GetAllPossibleAsPaths(this IEnumerable<ASEntry> asPath)
		{
			if (asPath is null)
			{
				throw new ArgumentNullException(nameof(asPath));
			}

			List<List<UInt32>> allAsPaths = new List<List<UInt32>>();

			if (asPath.Count() == 1 && !asPath.First().IsAsSet())
			{
				allAsPaths.Add(asPath.First().ASNumbers.ToList());

				return allAsPaths;
			}

			foreach (ASEntry asEntry in asPath)
			{
				if (asEntry.IsAsSet())
				{
					if (allAsPaths.Count == 0)
					{
						foreach (UInt32 asElem in asEntry.ASNumbers)
						{
							allAsPaths.Add(new List<UInt32>() { asElem });
						}
					}
					else
					{
						List<List<UInt32>> newList = new List<List<UInt32>>();

						foreach (UInt32 asSet in asEntry.ASNumbers)
						{
							List<List<UInt32>> originalList = allAsPaths.Clone();

							foreach (List<UInt32> path in originalList)
							{
								path.Add(asSet);
							}

							newList.AddRange(originalList);
						}

						allAsPaths = newList;
					}
				}
				else
				{
					if (allAsPaths.Count == 0)
					{
						allAsPaths.Add(asEntry.ASNumbers.ToList());
					}
					else
					{
						foreach (List<UInt32> path in allAsPaths)
						{
							path.AddRange(asEntry.ASNumbers.ToList());
						}
					}
				}
			}

			return allAsPaths;
		}

		public static bool HasAsPathWithoutLoops(this BGPMessageUpdate update)
		{
			if (update is null)
			{
				throw new ArgumentNullException(nameof(update));
			}

			if (update.ASPath is null || update.ASPath.Count == 0)
			{
				return false;
			}

			List<List<UInt32>> allPossibleAsPaths = update.GetAllPossibleAsPaths();

			foreach (List<UInt32> asPath in allPossibleAsPaths)
			{
				HashSet<UInt32> prevAses = new HashSet<UInt32>();

				UInt32 prevAs = asPath[0];
				prevAses.Add(asPath[0]);

				for (int i = 1; i < asPath.Count; i++)
				{
					if (prevAs != asPath[i])
					{
						if (!prevAses.Add(asPath[i]))
						{
							return false;
						}

						prevAs = asPath[i];
					}
				}
			}

			return true;
		}

		public static List<UInt32> GetASPathFlatten(this BGPMessageUpdate update, bool noPrepending = false)
		{
			if (update is null)
			{
				throw new ArgumentNullException(nameof(update));
			}

			if (update.ASPath == null || update.ASPath.Count == 0)
			{
				return null;
			}

			List<UInt32> newASpath = new List<UInt32>();

			if (noPrepending)
			{
				foreach (ASEntry entry in update.ASPath)
				{
					if (entry.IsAsSet())
					{
						if (newASpath.Count == 0 || newASpath.Last() != entry.ASNumbers[0])
						{
							newASpath.Add(entry.ASNumbers[0]);
						}
					}
					else
					{
						foreach (UInt32 element in entry.ASNumbers)
						{
							if (newASpath.Count == 0 || newASpath.Last() != element)
							{
								newASpath.Add(element);
							}
						}
					}
				}

				return newASpath;
			}

			foreach (ASEntry entry in update.ASPath)
			{
				if (entry.IsAsSet())
				{
					newASpath.Add(entry.ASNumbers[0]);
				}
				else
				{
					newASpath.AddRange(entry.ASNumbers);
				}
			}

			return newASpath;
		}
	}
}

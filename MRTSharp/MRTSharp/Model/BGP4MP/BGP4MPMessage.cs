﻿using MRTSharp.Model.BGP;
using MRTSharp.Model.BGP4MP.Constants;
using System;

namespace MRTSharp.Model.BGP4MP
{
	public class BGP4MPMessage : BGP4MPEntry, IEquatable<BGP4MPMessage>
	{
		public BGPMessage Message { get; set; }

		public BGP4MPMessage(BGP4MPSubtype subType) : base(subType)
		{
		}

		public override bool Equals(object obj)
		{
			return Equals(obj as BGP4MPMessage);
		}

		public bool Equals(BGP4MPMessage other)
		{
			return other != null &&
				   base.Equals(other) &&
				   Message.Equals(other.Message);
		}

		public override int GetHashCode()
		{
			return HashCode.Combine(base.GetHashCode(), Message.GetHashCode());
		}

		public override string ToString()
		{
			return base.ToString() + "\n" + Message.ToString();
		}
	}
}

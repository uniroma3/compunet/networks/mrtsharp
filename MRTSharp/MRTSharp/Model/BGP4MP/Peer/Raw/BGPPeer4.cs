﻿using MRTSharp.Model.IP;
using MRTSharp.Utils;
using System;

namespace MRTSharp.Model.BGP4MP.Peer.Raw
{
	public struct BGPPeer4 : IBGPPeer, IEquatable<BGPPeer4>
	{
#pragma warning disable CS0649 // Struct is filled with Marshal.Cast
		private UInt32 _peerAS;
		private UInt32 _localPeerAS;
		private UInt16 _interfaceNumber;
		private UInt16 _ipType;
#pragma warning restore CS0649

		public UInt32 GetPeerAS()
		{
			return (UInt32)ByteUtils.Swap(_peerAS);
		}

		public UInt32 GetLocalPeerAS()
		{
			return (UInt32)ByteUtils.Swap(_localPeerAS);
		}

		public UInt16 GetInterfaceNumber()
		{
			return ByteUtils.Swap(_interfaceNumber);
		}

		public IPType GetIPType()
		{
			return (IPType)ByteUtils.Swap(_ipType);
		}

		public void SetPeerAS(UInt32 peerAS)
		{
			_peerAS = ByteUtils.Swap(peerAS);
		}

		public void SetLocalPeerAS(UInt32 localPeerAS)
		{
			_localPeerAS = ByteUtils.Swap(localPeerAS);
		}

		public void SetInterfaceNumber(UInt16 interfaceNumber)
		{
			_interfaceNumber = interfaceNumber;
		}

		public void SetIPType(UInt16 ipType)
		{
			_ipType = ipType;
		}

		public override bool Equals(object obj)
		{
			return (obj is IBGPPeer peer && Equals(peer));
		}

		public bool Equals(BGPPeer4 other)
		{
			return _peerAS == other._peerAS &&
				   _localPeerAS == other._localPeerAS &&
				   _interfaceNumber == other._interfaceNumber &&
				   _ipType == other._ipType;
		}

		public bool Equals(IBGPPeer other)
		{
			return GetPeerAS() == other.GetPeerAS() &&
				   GetLocalPeerAS() == other.GetLocalPeerAS() &&
				   GetInterfaceNumber() == other.GetInterfaceNumber() &&
				   GetIPType() == other.GetIPType();
		}

		public override int GetHashCode()
		{
			return HashCode.Combine(_peerAS, _localPeerAS, _interfaceNumber, _ipType);
		}

		public static bool operator ==(BGPPeer4 left, BGPPeer4 right)
		{
			return left.Equals(right);
		}

		public static bool operator !=(BGPPeer4 left, BGPPeer4 right)
		{
			return !(left == right);
		}
	}
}

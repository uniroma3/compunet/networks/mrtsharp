﻿using System;

namespace MRTSharp.Model.BGP4MP.Constants
{
	public enum BGP4MPSubtype
	{
#pragma warning disable CA1707 // This are identifier as used in the RFC
		BGP4MP_STATE_CHANGE = 0,
		BGP4MP_MESSAGE = 1,
		BGP4MP_MESSAGE_AS4 = 4,
		BGP4MP_STATE_CHANGE_AS4 = 5,
		BGP4MP_MESSAGE_LOCAL = 6,
		BGP4MP_MESSAGE_AS4_LOCAL = 7,
		BGP4MP_MESSAGE_ADDPATH = 8,
		BGP4MP_MESSAGE_AS4_ADDPATH = 9,
		BGP4MP_MESSAGE_LOCAL_ADDPATH = 10,
		BGP4MP_MESSAGE_AS4_LOCAL_ADDPATH = 11,
#pragma warning restore CA1707
	}

	public static class BGP4MPSubtypeExtension
	{
		public static Byte GetASLength(this BGP4MPSubtype subType)
		{
			return subType switch
			{
				BGP4MPSubtype.BGP4MP_MESSAGE_AS4 or
				BGP4MPSubtype.BGP4MP_STATE_CHANGE_AS4 or
				BGP4MPSubtype.BGP4MP_MESSAGE_AS4_LOCAL or
				BGP4MPSubtype.BGP4MP_MESSAGE_AS4_ADDPATH or
				BGP4MPSubtype.BGP4MP_MESSAGE_AS4_LOCAL_ADDPATH => 4,
				BGP4MPSubtype.BGP4MP_STATE_CHANGE or
				BGP4MPSubtype.BGP4MP_MESSAGE or
				BGP4MPSubtype.BGP4MP_MESSAGE_LOCAL or
				BGP4MPSubtype.BGP4MP_MESSAGE_ADDPATH or
				BGP4MPSubtype.BGP4MP_MESSAGE_LOCAL_ADDPATH => 2,
				_ => throw new Exception("BGP4MP Message with subtype code " + subType + " not defined"),
			};
		}

	}
}

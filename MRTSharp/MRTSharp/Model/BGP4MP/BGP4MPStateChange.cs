﻿using MRTSharp.Model.BGP.Constants;
using MRTSharp.Model.BGP4MP.Constants;
using System;

namespace MRTSharp.Model.BGP4MP
{
	public class BGP4MPStateChange : BGP4MPEntry
	{
		public BGPStateStatus OldState { get; set; }
		public BGPStateStatus NewState { get; set; }

		public BGP4MPStateChange(BGP4MPSubtype subType) : base(subType)
		{
		}

		public override string ToString()
		{
			String result = base.ToString();
			result += "\tOLD_STATE: " + OldState.ToString();
			result += "\tNEW_STATE: " + NewState.ToString();
			return result;
		}
	}
}
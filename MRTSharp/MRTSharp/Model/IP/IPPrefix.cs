﻿using MRTSharp.Comparers;
using MRTSharp.Utils;
using System;
using System.Globalization;
using System.Linq;
using System.Net;
using System.Net.Sockets;

namespace MRTSharp.Model.IP
{
	public class IPPrefix : IEquatable<IPPrefix>
	{
		public int Cidr { get; set; }
		public byte[] Prefix { get; set; }
		public AddressFamily Family { get; set; }

		private readonly static ArrayEqualityComparer<byte> PrefixComparer = new();

		public IPPrefix(byte[] prefix, int cidr, AddressFamily addressFamily)
		{
			if (prefix is null)
			{
				throw new ArgumentNullException(nameof(prefix));
			}

			if ((cidr < 0 || cidr > 128) || (addressFamily == AddressFamily.InterNetwork && cidr > 32))
			{
				throw new FormatException("CIDR not valid.");
			}

			int bytesToRead = Convert.ToInt32(Math.Ceiling(cidr / 8.0d));

			if (prefix.Length != bytesToRead)
			{
				throw new ArgumentException("Prefix length is not compatible with CIDR.");
			}

			int bitsToClean = (bytesToRead * 8) - cidr;
			if (bitsToClean > 0)
			{
				bitsToClean--;

				byte bitMask = (byte)(0xff << bitsToClean);

				prefix[bytesToRead - 1] &= bitMask;
			}

			Prefix = prefix;
			Cidr = cidr;
			Family = addressFamily;
		}

		public IPAddress GetNetworkAddress()
		{
			byte length = IPUtils.GetIPAddressByteLength(Family);

			if (Prefix.Length == length)
			{
				return new IPAddress(Prefix);
			}
			else
			{
				byte[] fullPrefixArray = new byte[length];
				Array.Copy(Prefix, fullPrefixArray, Prefix.Length);
				return new IPAddress(fullPrefixArray);
			}
		}

		public static IPPrefix Parse(String stringPrefix)
		{
			if (string.IsNullOrEmpty(stringPrefix))
			{
				throw new ArgumentNullException(nameof(stringPrefix));
			}

			if (!stringPrefix.Contains('/'))
			{
				throw new FormatException("CIDR not defined.");
			}

			string[] parts = stringPrefix.Split("/");

			int cidr = Convert.ToInt32(parts[1], CultureInfo.InvariantCulture);

			IPAddress ipAddress = IPAddress.Parse(parts[0]);

			return IPPrefix.Parse(ipAddress, cidr);
		}

		public static IPPrefix Parse(IPAddress ipAddress, int cidr)
		{
			if (ipAddress is null)
			{
				throw new ArgumentNullException(nameof(ipAddress));
			}

			int bytesToRead = Convert.ToInt32(Math.Ceiling(cidr / 8.0d));

			return new IPPrefix(ipAddress.GetAddressBytes().Take(bytesToRead).ToArray(), cidr, ipAddress.AddressFamily);
		}

		public static bool TryParse(string stringPrefix, out IPPrefix prefix)
		{
			try
			{
				prefix = IPPrefix.Parse(stringPrefix);
			}
			catch
			{
				prefix = null;
				return false;
			}
			return true;
		}

		public static bool TryParse(IPAddress ipAddress, int cidr, out IPPrefix prefix)
		{
			try
			{
				prefix = IPPrefix.Parse(ipAddress, cidr);
			}
			catch
			{
				prefix = null;
				return false;
			}
			return true;
		}

		public override string ToString()
		{
			return GetNetworkAddress().ToString() + "/" + Cidr;
		}

		public override bool Equals(object obj)
		{
			return Equals(obj as IPPrefix);
		}

		public bool Equals(IPPrefix other)
		{
			// If parameter is null, return false.
			if (other is null)
			{
				return false;
			}

			// Optimization for a common success case.
			if (ReferenceEquals(this, other))
			{
				return true;
			}

			return GetType() == other.GetType() &&
					Cidr == other.Cidr &&
					Family == other.Family &&
					PrefixComparer.Equals(Prefix, other.Prefix);
		}

		public override int GetHashCode()
		{
			return HashCode.Combine(Cidr, PrefixComparer.GetHashCode(Prefix), Family);
		}
	}
}

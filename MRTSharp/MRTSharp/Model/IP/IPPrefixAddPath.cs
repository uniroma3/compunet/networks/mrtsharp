﻿using System;
using System.Net.Sockets;

namespace MRTSharp.Model.IP
{
	public class IPPrefixAddPath : IPPrefix
	{
		public UInt32 PathId { get; set; }

		public IPPrefixAddPath(byte[] prefix, int cidr, AddressFamily addressFamily, UInt32 pathId) : base(prefix, cidr, addressFamily)
		{
			PathId = pathId;
		}

		public override string ToString()
		{
			return base.ToString() + "\tPath ID: " + PathId;
		}

		public override bool Equals(object obj)
		{
			return Equals(obj as IPPrefixAddPath);
		}

		public bool Equals(IPPrefixAddPath other)
		{
			return other != null &&
				   base.Equals(other) &&
				   PathId == other.PathId;
		}

		public override int GetHashCode()
		{
			return HashCode.Combine(base.GetHashCode(), PathId);
		}
	}
}

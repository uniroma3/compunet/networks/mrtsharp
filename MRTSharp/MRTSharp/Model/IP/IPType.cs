﻿namespace MRTSharp.Model.IP
{
	/// <summary>
	/// IP Address Family.
	/// </summary>
	public enum IPType
	{
		IPv4 = 1,
		IPv6 = 2,
		Null = 3
	}
}

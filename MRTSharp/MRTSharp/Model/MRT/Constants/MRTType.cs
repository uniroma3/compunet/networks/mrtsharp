﻿namespace MRTSharp.Model.MRT.Constants
{
	public enum MRTType
	{
#pragma warning disable CA1707 // This are identifier as used in the RFC
		OSPFv2 = 11,
		TABLE_DUMP = 12,
		TABLE_DUMP_V2 = 13,
		BGP4MP = 16,
		BGP4MP_ET = 17,
		ISIS = 32,
		ISIS_ET = 33,
		OSPFv3 = 48,
		OSPFv3_ET = 49
#pragma warning restore CA1707
	}
}

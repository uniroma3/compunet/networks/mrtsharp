﻿using System;

namespace MRTSharp.Model.MRT
{
	public abstract class MRTEntry
	{
		public DateTime OriginateTime { get; set; }
	}
}

﻿using MRTSharp.Model.BGP4MP.Constants;
using MRTSharp.Model.MRT.Constants;
using MRTSharp.Utils;
using System;

namespace MRTSharp.Model.MRT
{
	public struct MRTHeader : IEquatable<MRTHeader>
	{
#pragma warning disable CS0649 // The fields are filled with a cast
		private readonly UInt32 _timestamp;

		private readonly UInt16 _type;

		private readonly UInt16 _subtype;

		private readonly UInt32 _length;

		private UInt32 _microsecondTimestamp;
#pragma warning restore CS0649

		public void SetMicrosecondTimestamp(UInt32 value)
		{
			_microsecondTimestamp = value;
		}

		public DateTime GetTimestamp()
		{
			return DateTime.UnixEpoch.AddSeconds(ByteUtils.Swap(_timestamp)).AddMilliseconds(ByteUtils.Swap(_microsecondTimestamp) / 1000);
		}

		public int GetLength()
		{
			return (int)ByteUtils.Swap(_length);
		}

		public MRTType GetMRTType()
		{
			return (MRTType)ByteUtils.Swap(_type);
		}

		public UInt16 GetSubtype()
		{
			return ByteUtils.Swap(_subtype);
		}

		public override bool Equals(object obj)
		{
			return obj is MRTHeader header && Equals(header);
		}

		public bool Equals(MRTHeader other)
		{
			return _timestamp == other._timestamp &&
				   _type == other._type &&
				   _subtype == other._subtype &&
				   _length == other._length;
		}

		public override int GetHashCode()
		{
			return HashCode.Combine(_timestamp, _type, _subtype, _length);
		}

		public static bool operator ==(MRTHeader left, MRTHeader right)
		{
			return left.Equals(right);
		}

		public static bool operator !=(MRTHeader left, MRTHeader right)
		{
			return !(left == right);
		}
	}
}

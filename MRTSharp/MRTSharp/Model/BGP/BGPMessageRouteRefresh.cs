﻿using MRTSharp.Model.BGP.Constants;

namespace MRTSharp.Model.BGP
{
	public class BGPMessageRouteRefresh : BGPMessage
	{
		public AFI Afi { get; set; }
		public SAFI Safi { get; set; }
	}
}

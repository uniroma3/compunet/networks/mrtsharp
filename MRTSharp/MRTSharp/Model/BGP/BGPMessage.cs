﻿using System;

namespace MRTSharp.Model.BGP
{
	public abstract class BGPMessage : IEquatable<BGPMessage>
	{
		public bool Malformed { get; set; }

		public override bool Equals(object obj)
		{
			return Equals(obj as BGPMessage);
		}

		public bool Equals(BGPMessage other)
		{
			// If parameter is null, return false.
			if (other is null)
			{
				return false;
			}

			// Optimization for a common success case.
			if (ReferenceEquals(this, other))
			{
				return true;
			}

			return GetType() == other.GetType() &&
					Malformed == other.Malformed;
		}

		public override int GetHashCode()
		{
			return Malformed.GetHashCode();
		}
	}
}

﻿using MRTSharp.Model.BGP.Capabilities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;

namespace MRTSharp.Model.BGP
{
	public class BGPMessageOpen : BGPMessage
	{
		public Byte BGPVersion { get; set; }
		public UInt32 ASNumber { get; set; }
		public TimeSpan HoldTime { get; set; }
		public IPAddress BGPIdentifier { get; set; }
		public List<IBGPCapability> Capabilities { get; } = new List<IBGPCapability>();

		public IEnumerable<T> Get<T>() where T : IBGPCapability
		{
			return Capabilities.OfType<T>();
		}

		public Boolean Has<T>() where T : IBGPCapability
		{
			return Get<T>().Any();
		}
	}
}

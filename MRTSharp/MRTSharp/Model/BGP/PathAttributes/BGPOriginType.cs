﻿namespace MRTSharp.Model.BGP.PathAttributes
{
	public enum BGPOriginType
	{
		IGP = 0,
		EGP = 1,
		INCOMPLETE = 2
	}
}

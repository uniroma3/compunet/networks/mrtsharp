﻿using System;
using System.Collections.Generic;
using System.Net;

namespace MRTSharp.Model.BGP.PathAttributes
{
	public class BGPAggregator : IEquatable<BGPAggregator>
	{
		public UInt32 ASNumber { get; set; }

		public IPAddress Address { get; set; }

		public override bool Equals(object obj)
		{
			return Equals(obj as BGPAggregator);
		}

		public bool Equals(BGPAggregator other)
		{
			// If parameter is null, return false.
			if (other is null)
			{
				return false;
			}

			// Optimization for a common success case.
			if (ReferenceEquals(this, other))
			{
				return true;
			}

			return ASNumber == other.ASNumber &&
				   EqualityComparer<IPAddress>.Default.Equals(Address, other.Address);
		}

		public override int GetHashCode()
		{
			return HashCode.Combine(ASNumber, Address);
		}

		public override string ToString()
		{
			return $"{ASNumber} {Address}";
		}

		public static bool operator ==(BGPAggregator left, BGPAggregator right)
		{
			return EqualityComparer<BGPAggregator>.Default.Equals(left, right);
		}

		public static bool operator !=(BGPAggregator left, BGPAggregator right)
		{
			return !(left == right);
		}
	}
}

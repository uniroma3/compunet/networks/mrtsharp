﻿namespace MRTSharp.Model.BGP.PathAttributes
{
	public enum ASPathSegmentType
	{
#pragma warning disable CA1707 // This are identifier as used in the RFC
		AS_SET = 1,
		AS_SEQUENCE = 2
#pragma warning restore CA1707
	}
}

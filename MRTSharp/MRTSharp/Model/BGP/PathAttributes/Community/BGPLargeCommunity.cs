﻿using System;

namespace MRTSharp.Model.BGP.PathAttributes
{
	public struct BGPLargeCommunity : IBGPCommunity, IEquatable<BGPLargeCommunity>
	{
		public UInt32 ASNumber { get; set; }
		public UInt32 LocalDataPart1 { get; set; }
		public UInt32 LocalDataPart2 { get; set; }

		public override string ToString()
		{
			return $"{ASNumber}:{LocalDataPart1}:{LocalDataPart2}";
		}

		public override bool Equals(object obj)
		{
			return obj is BGPLargeCommunity community && Equals(community);
		}

		public bool Equals(BGPLargeCommunity other)
		{
			return ASNumber == other.ASNumber &&
				   LocalDataPart1 == other.LocalDataPart1 &&
				   LocalDataPart2 == other.LocalDataPart2;
		}

		public override int GetHashCode()
		{
			return HashCode.Combine(ASNumber, LocalDataPart1, LocalDataPart2);
		}

		public static bool operator ==(BGPLargeCommunity left, BGPLargeCommunity right)
		{
			return left.Equals(right);
		}

		public static bool operator !=(BGPLargeCommunity left, BGPLargeCommunity right)
		{
			return !(left == right);
		}
	}
}

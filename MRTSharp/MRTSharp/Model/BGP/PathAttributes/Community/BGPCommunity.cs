﻿using System;

namespace MRTSharp.Model.BGP.PathAttributes
{
	public struct BGPCommunity : IBGPCommunity, IEquatable<BGPCommunity>
	{
		public UInt16 ASNumber { get; set; }
		public UInt16 Value { get; set; }

		public BGPCommunity(UInt16 asNumber, UInt16 value)
		{
			ASNumber = asNumber;
			Value = value;
		}

		public override string ToString()
		{
			return $"{ASNumber}:{Value}";
		}

		public override bool Equals(object obj)
		{
			return obj is BGPCommunity community && Equals(community);
		}

		public bool Equals(BGPCommunity other)
		{
			return ASNumber == other.ASNumber &&
				   Value == other.Value;
		}

		public override int GetHashCode()
		{
			return HashCode.Combine(ASNumber, Value);
		}

		public static bool operator ==(BGPCommunity left, BGPCommunity right)
		{
			return left.Equals(right);
		}

		public static bool operator !=(BGPCommunity left, BGPCommunity right)
		{
			return !(left == right);
		}
	}
}
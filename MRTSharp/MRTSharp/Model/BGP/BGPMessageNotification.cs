﻿using MRTSharp.Model.BGP.Constants;
using System;

namespace MRTSharp.Model.BGP
{
	public class BGPMessageNotification : BGPMessage
	{
		public BGPErrorCode ErrorCode { get; set; }
		public Byte ErrorSubCode { get; set; }
	}
}

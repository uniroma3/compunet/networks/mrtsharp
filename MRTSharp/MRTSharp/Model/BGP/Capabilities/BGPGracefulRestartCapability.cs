﻿using MRTSharp.Model.BGP.Constants;
using System;
using System.Collections.Generic;

namespace MRTSharp.Model.BGP.Capabilities
{
	public class BGPGracefulRestartCapability : IBGPCapability
	{
		public Boolean RestartState { get; set; }
		public TimeSpan RestartTime { get; set; }
		public Dictionary<(AFI, SAFI), Boolean> RestartFlags { get; set; }
	}
}

﻿using MRTSharp.Model.BGP.Constants;

namespace MRTSharp.Model.BGP.Capabilities
{
	public class BGPMultiProtocolCapability : IBGPCapability
	{
		public AFI Afi { get; set; }
		public SAFI Safi { get; set; }
	}
}

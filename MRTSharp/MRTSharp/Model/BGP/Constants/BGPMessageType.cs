﻿namespace MRTSharp.Model.BGP.Constants
{
	public enum BGPMessageType
	{
#pragma warning disable CA1707 // This are identifier as used in the RFC
		OPEN = 1,
		UPDATE = 2,
		NOTIFICATION = 3,
		KEEPALIVE = 4,
		ROUTE_REFRESH = 5
#pragma warning restore CA1707
	}
}

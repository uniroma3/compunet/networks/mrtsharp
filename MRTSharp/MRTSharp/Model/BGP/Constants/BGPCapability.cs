﻿namespace MRTSharp.Model.BGP.Constants
{
	public enum BGPCapability
	{
#pragma warning disable CA1707 // This are identifier as used in the RFC
		MULTIPROTOCOL = 1,
		ROUTE_REFRESH = 2,
		EXTENDED_MESSAGE = 6,
		ROLE = 9,
		GRACEFUL_RESTART = 64,
		SUPPORT_4_ASN = 65,
		DYNAMIC_CAPABILITY = 67,
		ADD_PATH = 69,
		ENHANCED_ROUTE_REFRESH = 70,
		LLGR = 71,
		FQDN = 73,
		PATHS_LIMIT = 76,
		PRESTANDARD_ROUTE_REFRESH = 128
#pragma warning restore CA1707
	}
}

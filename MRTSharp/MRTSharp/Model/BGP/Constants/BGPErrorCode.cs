﻿namespace MRTSharp.Model.BGP.Constants
{
	public enum BGPErrorCode
	{
#pragma warning disable CA1707 // This are identifier as used in the RFC
		MESSAGE_HEADER = 1,
		OPEN = 2,
		UPDATE = 3,
		HOLD_TIMER_EXPIRED = 4,
		FSM_ERROR = 5,
		CEASE = 6,
#pragma warning restore CA1707
	}
}

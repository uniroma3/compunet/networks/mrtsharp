﻿namespace MRTSharp.Model.BGP.Constants
{
	public enum BGPStateStatus
	{
		Unknown = 0,
		Idle = 1,
		Connect = 2,
		Active = 3,
		OpenSent = 4,
		OpenConfirm = 5,
		Established = 6,
		Clearing = 7,
		Deleted = 8
	}
}
﻿namespace MRTSharp.Model.TableDump.Constants
{
	public enum TableDumpSubtype
	{
#pragma warning disable CA1707 // This are identifier as used in the RFC
		PEER_INDEX_TABLE = 1,
		RIB_IPV4_UNICAST = 2,
		RIB_IPV4_MULTICAST = 3,
		RIB_IPV6_UNICAST = 4,
		RIB_IPV6_MULTICAST = 5,
		RIB_GENERIC = 6,
		GEO_PEER_TABLE = 7,
		RIB_IPV4_UNICAST_ADDPATH = 8,
		RIB_IPV4_MULTICAST_ADDPATH = 9,
		RIB_IPV6_UNICAST_ADDPATH = 10,
		RIB_IPV6_MULTICAST_ADDPATH = 11,
		RIB_GENERIC_ADDPATH = 12
#pragma warning restore CA1707
	}
}

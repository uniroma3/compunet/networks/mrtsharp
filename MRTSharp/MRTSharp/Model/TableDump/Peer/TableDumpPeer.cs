using System;
using System.Net;

namespace MRTSharp.Model.TableDump.Peer
{
	public class TableDumpPeer : IEquatable<TableDumpPeer>
	{
		public IPAddress IpAddress { get; set; }
		public UInt32 PeerAS { get; set; }

		public override bool Equals(object obj)
		{
			return obj is TableDumpPeer peer && Equals(peer);
		}

		public bool Equals(TableDumpPeer other)
		{
			return other != null &&
					IpAddress == other.IpAddress &&
					PeerAS == other.PeerAS;
		}

		public override int GetHashCode()
		{
			return HashCode.Combine(IpAddress, PeerAS);
		}

		public override string ToString()
		{
			return $"AS{PeerAS} {IpAddress}";
		}

		public static bool operator ==(TableDumpPeer left, TableDumpPeer right)
		{
			return left is not null &&
					left.Equals(right);
		}

		public static bool operator !=(TableDumpPeer left, TableDumpPeer right)
		{
			return !(left == right);
		}
	}
}

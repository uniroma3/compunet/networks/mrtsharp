﻿using MRTSharp.Model.BGP;
using MRTSharp.Model.TableDump.Peer;
using System;

namespace MRTSharp.Model.TableDump
{
	public class TableDumpRIBEntry
	{
		public DateTime OriginateTime { get; set; }
		public TableDumpPeer Peer { get; set; }
		public UInt32? PathId { get; set; }
		public BGPMessageUpdate BGPUpdate { get; set; }

		public TableDumpRIBEntry(TableDumpPeer peer, DateTime originateTime, BGPMessageUpdate bgpUpdate)
		{
			OriginateTime = originateTime;
			Peer = peer;
			BGPUpdate = bgpUpdate;
		}
	}
}
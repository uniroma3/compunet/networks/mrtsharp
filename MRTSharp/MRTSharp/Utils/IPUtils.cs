﻿using MRTSharp.File;
using MRTSharp.Model.IP;
using System;
using System.Net;
using System.Net.Sockets;

namespace MRTSharp.Utils
{
	internal static class IPUtils
	{
		public static IPAddress ReadIPAddress(FileBuffer buffer, IPType ipType)
		{
			if (ipType == IPType.Null)
			{
				throw new ArgumentNullException(nameof(ipType));
			}

			int bytesNumber = GetIPAddressByteLength(GetAddressFamily(ipType));

			return new IPAddress(buffer.ReadBytes(bytesNumber));
		}

		public static IPPrefix ReadPrefix(FileBuffer buffer, IPType ipType, Boolean addPath = false)
		{
			UInt32 pathId = 0;
			if (addPath)
			{
				pathId = buffer.ReadUInt32();
			}

			byte subnetMask = buffer.ReadByte(); //SubnetMask, in BIT, if == 0 the prefix is the default route

			if ((subnetMask < 0 || subnetMask > 128) || (ipType == IPType.IPv4 && subnetMask > 32))
			{
				throw new FormatException("CIDR not valid.");
			}

			byte[] prefix;
			if (subnetMask > 0)
			{
				int bytesToRead = Convert.ToInt32(Math.Ceiling(subnetMask / 8.0d));

				prefix = buffer.ReadBytes(bytesToRead).ToArray();
			}
			else
			{
				prefix = Array.Empty<Byte>();
			}

			AddressFamily af = GetAddressFamily(ipType);
			return (addPath) ? new IPPrefixAddPath(prefix, subnetMask, af, pathId) : new IPPrefix(prefix, subnetMask, af);
		}

		public static Byte GetIPAddressByteLength(AddressFamily family)
		{
			if (family != AddressFamily.InterNetwork && family != AddressFamily.InterNetworkV6)
			{
				throw new Exception();
			}

			return AddressFamily.InterNetwork == family ? (byte)4 : (byte)16;
		}

		public static AddressFamily GetAddressFamily(IPType ipType)
		{
			switch (ipType)
			{
				case IPType.IPv4:
					{
						return AddressFamily.InterNetwork;
					}
				case IPType.IPv6:
					{
						return AddressFamily.InterNetworkV6;
					}
				default:
					{
						throw new Exception("Error in IPType");
					}
			}
		}
	}
}

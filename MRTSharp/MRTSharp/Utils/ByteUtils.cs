﻿using System;
using System.Collections.Generic;
using System.Text;

namespace MRTSharp.Utils
{
	public static class ByteUtils
	{
		public static UInt32 Swap(UInt32 x)
		{
			return unchecked((((x) << 24) | (((x) << 8) & 0x00ff0000) | (((x) >> 8) & 0x0000ff00) | ((x) >> 24)));
		}

		public static UInt16 Swap(UInt16 x)
		{
			return unchecked((UInt16)((((x) & 0xff) << 8) | ((UInt16)(x) >> 8)));
		}
	}
}

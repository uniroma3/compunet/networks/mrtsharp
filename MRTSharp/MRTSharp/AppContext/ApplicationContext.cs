﻿using Microsoft.Extensions.Logging;

namespace MRTSharp.AppContext
{
	internal class ApplicationContext
	{
		public ILogger Logger { get; set; }
		public bool ThrowExceptions { get; set; } = false;
	}
}

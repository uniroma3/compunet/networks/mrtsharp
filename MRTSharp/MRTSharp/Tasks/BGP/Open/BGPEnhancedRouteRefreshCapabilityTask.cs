﻿using MRTSharp.Exceptions;
using MRTSharp.Model.BGP.Capabilities;
using MRTSharp.Model.BGP.Constants;
using System;

namespace MRTSharp.Tasks.BGP.Open
{
	internal static class BGPEnhancedRouteRefreshCapabilityTask
	{
		internal static BGPEnhancedRouteRefreshCapability Run(UInt16 capabilityLength)
		{
			if (capabilityLength != 0)
			{
				throw new CapabilityLenghtMalformed(BGPCapability.ENHANCED_ROUTE_REFRESH, 0, capabilityLength);
			}

			return new BGPEnhancedRouteRefreshCapability();
		}
	}
}

﻿using MRTSharp.Exceptions;
using MRTSharp.File;
using MRTSharp.Model.BGP;
using MRTSharp.Model.BGP.Capabilities;
using MRTSharp.Model.BGP.Constants;
using System;

namespace MRTSharp.Tasks.BGP.Open
{
	internal static class BGPAS4SupportCapabilityTask
	{
		private const int AS4SupportCapabilityLength = 4;
		private const UInt32 AS_TRANS = 23456;

		internal static BGPAS4SupportCapability Run(FileBuffer buffer, UInt16 capabilityLength, BGPMessageOpen bgpMessageOpen)
		{
			if (capabilityLength != AS4SupportCapabilityLength)
			{
				throw new CapabilityLenghtMalformed(BGPCapability.SUPPORT_4_ASN, AS4SupportCapabilityLength, capabilityLength);
			}

			UInt32 asNumber = buffer.ReadUInt32();

			if (asNumber != bgpMessageOpen.ASNumber)
			{
				if (bgpMessageOpen.ASNumber != AS_TRANS)
				{
					throw new Exception("AS_NUMBER not AS_TRANS");
				}
				else
				{
					bgpMessageOpen.ASNumber = asNumber;
				}
			}

			return new BGPAS4SupportCapability();
		}
	}
}

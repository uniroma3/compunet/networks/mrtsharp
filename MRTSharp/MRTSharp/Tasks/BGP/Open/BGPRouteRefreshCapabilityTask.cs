﻿using MRTSharp.Exceptions;
using MRTSharp.Model.BGP.Capabilities;
using MRTSharp.Model.BGP.Constants;
using System;

namespace MRTSharp.Tasks.BGP.Open
{
	internal static class BGPRouteRefreshCapabilityTask
	{
		internal static BGPRouteRefreshCapability Run(UInt16 capabilityLength)
		{
			if (capabilityLength != 0)
			{
				throw new CapabilityLenghtMalformed(BGPCapability.ROUTE_REFRESH, 0, capabilityLength);
			}

			return new BGPRouteRefreshCapability();
		}
	}
}

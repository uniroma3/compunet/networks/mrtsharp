﻿using MRTSharp.Exceptions;
using MRTSharp.File;
using MRTSharp.Model.BGP.Capabilities;
using MRTSharp.Model.BGP.Constants;
using System;

namespace MRTSharp.Tasks.BGP.Open
{
	internal static class BGPLLGRCapabilityTask
	{
		internal static BGPLLGRCapability Run(FileBuffer buffer, UInt16 capabilityLength)
		{
			if (capabilityLength % 7 != 0)
			{
				throw new CapabilityLenghtMalformed(BGPCapability.LLGR, "expected 0 or multiple of 7", capabilityLength);
			}

			buffer.Seek(capabilityLength);

			return new BGPLLGRCapability();
		}
	}
}

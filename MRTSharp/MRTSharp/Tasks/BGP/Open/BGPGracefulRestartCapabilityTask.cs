﻿using MRTSharp.Exceptions;
using MRTSharp.File;
using MRTSharp.Model.BGP.Capabilities;
using MRTSharp.Model.BGP.Constants;
using MRTSharp.Tasks.BGP.Common;
using System;
using System.Collections.Generic;

namespace MRTSharp.Tasks.BGP.Open
{
	internal static class BGPGracefulRestartCapabilityTask
	{
		internal static BGPGracefulRestartCapability Run(FileBuffer buffer, UInt16 capabilityLength)
		{
			UInt16 gracefulNotification = buffer.ReadUInt16();

			BGPGracefulRestartCapability gracefulRestartCapability = new BGPGracefulRestartCapability()
			{
				RestartState = (gracefulNotification & 0x8000) == 1,
				RestartTime = TimeSpan.FromSeconds(gracefulNotification & 0x0FFF)
			};

			capabilityLength -= sizeof(UInt16);
			if (capabilityLength % sizeof(UInt32) != 0)
				throw new CapabilityLenghtMalformed(BGPCapability.GRACEFUL_RESTART, $"multiple of {sizeof(UInt32)}", capabilityLength);

			if (capabilityLength > 0)
			{
				gracefulRestartCapability.RestartFlags = new Dictionary<(AFI, SAFI), Boolean>();

				while (capabilityLength > 0)
				{
					(AFI afi, SAFI safi) = BGPAFISAFITask.Run(buffer, ref capabilityLength);
					Byte flags = buffer.ReadByte();
					capabilityLength -= sizeof(Byte);

					gracefulRestartCapability.RestartFlags.Add((afi, safi), (flags & 0x01) == 1);
				}
			}

			return gracefulRestartCapability;
		}
	}
}

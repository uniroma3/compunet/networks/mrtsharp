﻿using MRTSharp.File;
using MRTSharp.Model.BGP;
using MRTSharp.Model.BGP.Constants;
using MRTSharp.Model.IP;
using MRTSharp.Tasks.BGP.Update;
using MRTSharp.Utils;
using System;
using System.Collections.Generic;
using System.Net;

namespace MRTSharp.Tasks.BGP.NLRI
{
	internal static class BGPIPUnicastNLRITask
	{
		public static void ParseNextHops(IPType ipType, FileBuffer buffer, ref BGPMessageUpdate bgpUpdate, Byte nextHopLength, UInt16 flags)
		{
			if (nextHopLength > 0 && bgpUpdate.NextHops is null)
			{
				bgpUpdate.NextHops = new List<IPAddress>();
			}

			Byte afiByteLength = IPUtils.GetIPAddressByteLength(IPUtils.GetAddressFamily(ipType));
			if (nextHopLength % afiByteLength != 0)
			{
				throw new Exception($"NLRI Next Hop Length not a divisor of {AFI.IPv6}");
			}

			while (nextHopLength > 0)
			{
				bgpUpdate.NextHops.Add(IPUtils.ReadIPAddress(buffer, ipType));

				nextHopLength -= afiByteLength;
			}
		}

		public static void ParseNLRI(IPType ipType, FileBuffer buffer, ref BGPMessageUpdate bgpUpdate, ref List<IPPrefix> nlriList, ref UInt16 attributeLength, UInt16 flags)
		{
			try
			{
				BGP4MPMessageUpdatePrefixesTask.Run(buffer, nlriList, ref attributeLength, ipType, flags);
			}
			catch (FormatException)
			{
				bgpUpdate.Malformed = true;
				buffer.Seek(--attributeLength);
			}
		}
	}
}

﻿using Microsoft.Extensions.Logging;
using MRTSharp.AppContext;
using MRTSharp.File;
using MRTSharp.Model.BGP;
using MRTSharp.Model.BGP.Constants;
using MRTSharp.Tasks.BGP.Update.PathAttributes;
using MRTSharp.Utils;
using System;

namespace MRTSharp.Tasks.BGP.Update
{
	internal static class BGP4MPMessageUpdateAttributesTask
	{
		internal static void Run(FileBuffer buffer, ref BGPMessageUpdate bgpUpdate, ref UInt16 messageLength, UInt16 flags)
		{
			int totalAttributesLength = (int)buffer.ReadUInt16();

			messageLength -= sizeof(UInt16);
			messageLength -= (UInt16)totalAttributesLength;

			ParseAttributes(buffer, ref bgpUpdate, totalAttributesLength, flags);
		}

		internal static void ParseAttributes(FileBuffer buffer, ref BGPMessageUpdate bgpUpdate, int totalAttributesLength, UInt16 flags)
		{
			while (totalAttributesLength > 0)
			{
				Byte attributeFlags = buffer.ReadByte();
				bool extendedLength = (attributeFlags & 0x10) != 0;

				BGPPathAttribute attributeTypeCode = (BGPPathAttribute)buffer.ReadByte();
				UInt16 attributeLength = (extendedLength) ? buffer.ReadUInt16() : buffer.ReadByte();

				if (attributeLength > totalAttributesLength)
				{
					throw new Exception("Attribute is bigger than remaining message length");
				}

				IBGPAttributeTask attributeInstance = (attributeTypeCode) switch
				{
					BGPPathAttribute.ORIGIN => Singleton<BGPOriginAttributeTask>.Instance,
					BGPPathAttribute.AS_PATH => Singleton<BGPASPathAttributeTask>.Instance,
					BGPPathAttribute.NEXT_HOP => Singleton<BGPNextHopAttributeTask>.Instance,
					BGPPathAttribute.MULTI_EXIT_DISC => Singleton<BGPMEDAttributeTask>.Instance,
					BGPPathAttribute.LOCAL_PREF => Singleton<BGPLocalPrefAttributeTask>.Instance,
					BGPPathAttribute.ATOMIC_AGGREGATE => Singleton<BGPAtomicAggregateAttributeTask>.Instance,
					BGPPathAttribute.AGGREGATOR => Singleton<BGPAggregatorAttributeTask>.Instance,
					BGPPathAttribute.COMMUNITY => Singleton<BGPCommunityAttributeTask>.Instance,
					BGPPathAttribute.ORIGINATOR_ID => Singleton<BGPOriginatorIDAttributeTask>.Instance,
					BGPPathAttribute.CLUSTER_LIST => Singleton<BGPClusterListAttributeTask>.Instance,
					BGPPathAttribute.MP_REACH_NLRI => Singleton<BGPReachNLRIAttributeTask>.Instance,
					BGPPathAttribute.MP_UNREACH_NLRI => Singleton<BGPUnreachNLRIAttributeTask>.Instance,
					BGPPathAttribute.EXTENDED_COMMUNITY => Singleton<BGPExtendedCommunityAttributeTask>.Instance,
					BGPPathAttribute.AS4_PATH => Singleton<BGPAS4PathAttributeTask>.Instance,
					BGPPathAttribute.AS4_AGGREGATOR => Singleton<BGPAS4AggregatorAttributeTask>.Instance,
					BGPPathAttribute.AS_PATHLIMIT => Singleton<BGPASPathLimitAttributeTask>.Instance,
					BGPPathAttribute.LARGE_COMMUNITY => Singleton<BGPLargeCommunityAttributeTask>.Instance,
					BGPPathAttribute.ATTR_SET => Singleton<BGPAttrSetAttributeTask>.Instance,
					_ => new Func<IBGPAttributeTask>(() =>
					{
						Singleton<ApplicationContext>.Instance.Logger?.LogDebug("Path Attribute {attributeTypeCode} not parsed", attributeTypeCode);

						return Singleton<BGPNoopAttributeTask>.Instance;
					}).Invoke()
				};

				if (attributeInstance is not null)
				{
					attributeInstance.Run(buffer, ref bgpUpdate, attributeLength, flags);
				}

				totalAttributesLength -= sizeof(Byte) + sizeof(Byte) + ((extendedLength) ? sizeof(UInt16) : sizeof(Byte)) + attributeLength;
			}

			/* Message is malformed, seek back to the end of the message */
			if (totalAttributesLength < 0)
			{
				bgpUpdate.Malformed = true;
				buffer.Seek(totalAttributesLength);
			}
		}
	}
}

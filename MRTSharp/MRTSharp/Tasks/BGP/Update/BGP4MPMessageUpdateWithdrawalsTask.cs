﻿using MRTSharp.File;
using MRTSharp.Model.BGP;
using MRTSharp.Model.IP;
using System;
using System.Collections.Generic;

namespace MRTSharp.Tasks.BGP.Update
{
	internal static class BGP4MPMessageUpdateWithdrawalsTask
	{
		internal static void Run(FileBuffer buffer, ref BGPMessageUpdate bgpUpdate, ref UInt16 messageLength, UInt16 flags)
		{
			UInt16 withdrawRoutesLength = buffer.ReadUInt16();
			if (withdrawRoutesLength > 0)
			{
				if (bgpUpdate.Withdrawals is null)
				{
					bgpUpdate.Withdrawals = new List<IPPrefix>();
				}

				UInt16 routesLength = withdrawRoutesLength;
				BGP4MPMessageUpdatePrefixesTask.Run(buffer, bgpUpdate.Withdrawals, ref routesLength, IPType.IPv4, flags);
			}

			messageLength -= sizeof(UInt16);
			messageLength -= withdrawRoutesLength;
		}
	}
}

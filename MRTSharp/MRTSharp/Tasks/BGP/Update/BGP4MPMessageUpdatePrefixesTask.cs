﻿using MRTSharp.File;
using MRTSharp.Model.IP;
using MRTSharp.Utils;
using System;
using System.Collections.Generic;

namespace MRTSharp.Tasks.BGP.Update
{
	internal static class BGP4MPMessageUpdatePrefixesTask
	{
		internal static void Run(FileBuffer buffer, List<IPPrefix> prefixes, ref UInt16 length, IPType ipType, UInt16 flags)
		{
			Boolean addPath = FlagUtils.GetAddPath(flags);

			while (length > 0)
			{
				IPPrefix prefix = IPUtils.ReadPrefix(buffer, ipType, addPath: addPath);
				length -= (UInt16)(Math.Ceiling(prefix.Cidr / 8.0d) + 1);
				length -= (addPath) ? (ushort)sizeof(UInt32) : (ushort)0;

				prefixes.Add(prefix);
			}
		}
	}
}

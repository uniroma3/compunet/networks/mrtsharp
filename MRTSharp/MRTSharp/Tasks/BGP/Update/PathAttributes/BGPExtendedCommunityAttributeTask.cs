﻿using MRTSharp.File;
using MRTSharp.Model.BGP;
using MRTSharp.Model.BGP.PathAttributes;
using System;
using System.Collections.Generic;

namespace MRTSharp.Tasks.BGP.Update.PathAttributes
{
	internal class BGPExtendedCommunityAttributeTask : IBGPAttributeTask
	{
		private const int ExtendedCommunityLength = 8;

		public void Run(FileBuffer buffer, ref BGPMessageUpdate bgpUpdate, UInt16 attributeLength, UInt16 flags)
		{
			if (attributeLength % ExtendedCommunityLength != 0)
			{
				throw new Exception("Community field length malformed");
			}

			int nCommunities = attributeLength / ExtendedCommunityLength;
			if (nCommunities > 0)
			{
				bgpUpdate.Communities ??= new List<IBGPCommunity>(nCommunities);

				for (int i = 0; i < nCommunities; ++i)
				{
					Byte type = buffer.ReadByte();
					Boolean extendedType = IsExtendedType((Byte)(type & 0x3f));
					Byte? subType = (extendedType) ? buffer.ReadByte() : null;

					BGPExtendedCommunity community = new()
					{
						Type = type,
						SubType = subType,
						Value = buffer.ReadBytes(ExtendedCommunityLength - ((extendedType) ? sizeof(UInt16) : sizeof(Byte))).ToArray()
					};

					bgpUpdate.Communities.Add(community);
				}
			}
		}

		private static Boolean IsExtendedType(Byte type)
		{
			return type == 0x00 ||
					type == 0x01 ||
					type == 0x02 ||
					type == 0x03 ||
					type == 0x06 ||
					type == 0x0b;
		}
	}
}

﻿using MRTSharp.File;
using MRTSharp.Model.BGP;
using MRTSharp.Model.IP;
using MRTSharp.Utils;
using System;

namespace MRTSharp.Tasks.BGP.Update.PathAttributes
{
	internal class BGPOriginatorIDAttributeTask : IBGPAttributeTask
	{
		private const int OriginatorIDLength = 4;

		public void Run(FileBuffer buffer, ref BGPMessageUpdate bgpUpdate, UInt16 attributeLength, UInt16 flags)
		{
			if (attributeLength != OriginatorIDLength)
			{
				throw new Exception("ORIGINATOR_ID length malformed");
			}

			bgpUpdate.OriginatorID = IPUtils.ReadIPAddress(buffer, IPType.IPv4);
		}
	}
}

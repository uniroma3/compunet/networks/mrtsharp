﻿using MRTSharp.File;
using MRTSharp.Model.BGP;
using System;

namespace MRTSharp.Tasks.BGP.Update.PathAttributes
{
	internal interface IBGPAttributeTask
	{
		public void Run(FileBuffer buffer, ref BGPMessageUpdate bgpUpdate, UInt16 attributeLength, UInt16 flags);
	}
}

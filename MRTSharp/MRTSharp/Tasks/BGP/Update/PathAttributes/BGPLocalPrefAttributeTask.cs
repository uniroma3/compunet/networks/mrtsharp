﻿using MRTSharp.File;
using MRTSharp.Model.BGP;
using System;

namespace MRTSharp.Tasks.BGP.Update.PathAttributes
{
	internal class BGPLocalPrefAttributeTask : IBGPAttributeTask
	{
		private const int LocalPrefLength = 4;

		public void Run(FileBuffer buffer, ref BGPMessageUpdate bgpUpdate, UInt16 attributeLength, UInt16 flags)
		{
			if (attributeLength != LocalPrefLength)
			{
				throw new Exception("LOCAL_PREF field length malformed");
			}

			bgpUpdate.LocalPreference = buffer.ReadUInt32();
		}
	}
}

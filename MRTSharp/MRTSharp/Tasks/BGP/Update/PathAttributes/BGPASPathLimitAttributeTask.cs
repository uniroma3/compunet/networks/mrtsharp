﻿using MRTSharp.File;
using MRTSharp.Model.BGP;
using MRTSharp.Model.BGP.PathAttributes;
using System;

namespace MRTSharp.Tasks.BGP.Update.PathAttributes
{
	internal class BGPASPathLimitAttributeTask : IBGPAttributeTask
	{
		private const int ASPathLimitLength = 5;

		public void Run(FileBuffer buffer, ref BGPMessageUpdate bgpUpdate, UInt16 attributeLength, UInt16 flags)
		{
			if (attributeLength != ASPathLimitLength)
			{
				throw new Exception("AS Path Limit field length malformed");
			}

			bgpUpdate.AsPathLimit = buffer.ReadAsType<BGPASPathLimit>(ASPathLimitLength)[0];
		}
	}
}

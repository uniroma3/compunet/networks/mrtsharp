﻿using MRTSharp.File;
using MRTSharp.Model.BGP;
using MRTSharp.Model.BGP.PathAttributes;
using MRTSharp.Model.IP;
using MRTSharp.Utils;
using System;

namespace MRTSharp.Tasks.BGP.Update.PathAttributes
{
	internal abstract class BGPAbstractAggregatorAttributeTask : IBGPAttributeTask
	{
		public void Run(FileBuffer buffer, ref BGPMessageUpdate bgpUpdate, UInt16 attributeLength, UInt16 flags)
		{
			TestLength(attributeLength, FlagUtils.GetASByteLength(flags));

			bgpUpdate.Aggregator = new BGPAggregator
			{
				ASNumber = ReadASNumber(buffer, attributeLength),
				Address = IPUtils.ReadIPAddress(buffer, IPType.IPv4)
			};
		}

		internal abstract void TestLength(UInt16 attributeLength, Byte asByteLength);

		internal abstract UInt32 ReadASNumber(FileBuffer buffer, UInt16 attributeLength);
	}
}

﻿using MRTSharp.File;
using System;

namespace MRTSharp.Tasks.BGP.Update.PathAttributes
{
	internal class BGPAggregatorAttributeTask : BGPAbstractAggregatorAttributeTask
	{
		private const int AS2AggregatorLength = 6;
		private const int AS4AggregatorLength = 8;

		internal override void TestLength(UInt16 attributeLength, Byte asByteLength)
		{
			if ((asByteLength == 2 && attributeLength != AS2AggregatorLength) || (asByteLength == 4 && attributeLength != AS4AggregatorLength))
			{
				throw new Exception("Aggregator field length malformed");
			}
		}

		internal override UInt32 ReadASNumber(FileBuffer buffer, UInt16 attributeLength)
		{
			return (attributeLength == AS4AggregatorLength) ? buffer.ReadUInt32() : buffer.ReadUInt16();
		}
	}
}

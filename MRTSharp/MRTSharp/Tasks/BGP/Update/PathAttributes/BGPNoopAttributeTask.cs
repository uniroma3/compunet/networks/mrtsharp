﻿using MRTSharp.File;
using MRTSharp.Model.BGP;
using System;

namespace MRTSharp.Tasks.BGP.Update.PathAttributes
{
	internal class BGPNoopAttributeTask : IBGPAttributeTask
	{
		public void Run(FileBuffer buffer, ref BGPMessageUpdate bgpUpdate, ushort attributeLength, UInt16 flags)
		{
			buffer.Seek(attributeLength);
		}
	}
}

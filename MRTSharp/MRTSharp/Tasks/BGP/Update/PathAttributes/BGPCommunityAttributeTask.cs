﻿using MRTSharp.File;
using MRTSharp.Model.BGP;
using MRTSharp.Model.BGP.PathAttributes;
using MRTSharp.Utils;
using System;
using System.Collections.Generic;

namespace MRTSharp.Tasks.BGP.Update.PathAttributes
{
	internal class BGPCommunityAttributeTask : IBGPAttributeTask
	{
		private const int CommunityLength = 4;

		public void Run(FileBuffer buffer, ref BGPMessageUpdate bgpUpdate, UInt16 attributeLength, UInt16 flags)
		{
			if (attributeLength % CommunityLength != 0)
			{
				throw new Exception("Community field length malformed");
			}

			int nCommunities = attributeLength / CommunityLength;
			if (nCommunities > 0)
			{
				bgpUpdate.Communities ??= new List<IBGPCommunity>(nCommunities);

				ReadOnlySpan<BGPCommunity> communities = buffer.ReadAsType<BGPCommunity>(attributeLength);

				for (int i = 0; i < nCommunities; ++i)
				{
					BGPCommunity community = communities[i];

					community.ASNumber = ByteUtils.Swap(community.ASNumber);
					community.Value = ByteUtils.Swap(community.Value);

					bgpUpdate.Communities.Add(community);
				}
			}
		}
	}
}

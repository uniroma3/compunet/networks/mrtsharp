using MRTSharp.File;
using MRTSharp.Model.BGP;
using MRTSharp.Model.BGP.Constants;
using MRTSharp.Model.IP;
using MRTSharp.Tasks.BGP.NLRI;
using System;
using System.Collections.Generic;

namespace MRTSharp.Tasks.BGP.Update.PathAttributes
{
	internal class BGPReachNLRIAttributeTask : BGPAbstractNLRIAttributeTask
	{
		private const int reservedLength = 1;

		internal override void ParseNextHops(FileBuffer buffer, ref BGPMessageUpdate bgpUpdate, ref UInt16 attributeLength, (AFI afi, SAFI safi) afiSafi, UInt16 flags)
		{
			Byte nextHopLength = buffer.ReadByte();
			attributeLength--;

			switch (afiSafi)
			{
				/* Add here other supported AFI/SAFIs */
				case (AFI.IPv4, SAFI.UNICAST):
					BGPIPUnicastNLRITask.ParseNextHops(IPType.IPv4, buffer, ref bgpUpdate, nextHopLength, flags);

					break;
				case (AFI.IPv6, SAFI.UNICAST):
					BGPIPUnicastNLRITask.ParseNextHops(IPType.IPv6, buffer, ref bgpUpdate, nextHopLength, flags);

					break;
				default:
					/* AFI/SAFI Not supported */
					throw new Exception($"AFI/SAFI={afiSafi} not supported");
			}

			buffer.Seek(reservedLength);
			attributeLength -= (UInt16)(nextHopLength + reservedLength);
		}

		internal override List<IPPrefix> GetNLRIList(ref BGPMessageUpdate bgpUpdate, ushort attributeLength)
		{
			if (attributeLength > 0 && bgpUpdate.Announcements is null)
			{
				bgpUpdate.Announcements = new List<IPPrefix>();
			}

			return bgpUpdate.Announcements;
		}
	}
}

﻿using MRTSharp.File;
using MRTSharp.Model.BGP;
using System;

namespace MRTSharp.Tasks.BGP.Update.PathAttributes
{
	internal class BGPAtomicAggregateAttributeTask : IBGPAttributeTask
	{
		public void Run(FileBuffer buffer, ref BGPMessageUpdate bgpUpdate, UInt16 attributeLength, UInt16 flags)
		{
			if (attributeLength != 0)
			{
				throw new Exception("Atomc Aggregator field length malformed");
			}

			bgpUpdate.AtomicAggregate = true;
		}
	}
}

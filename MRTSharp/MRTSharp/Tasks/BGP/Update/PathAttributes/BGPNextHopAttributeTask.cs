﻿using MRTSharp.File;
using MRTSharp.Model.BGP;
using MRTSharp.Model.IP;
using MRTSharp.Utils;
using System;
using System.Collections.Generic;
using System.Net;

namespace MRTSharp.Tasks.BGP.Update.PathAttributes
{
	internal class BGPNextHopAttributeTask : IBGPAttributeTask
	{
		private const int NextHopLength = 4;

		public void Run(FileBuffer buffer, ref BGPMessageUpdate bgpUpdate, UInt16 attributeLength, UInt16 flags)
		{
			if (attributeLength != NextHopLength)
			{
				throw new Exception("Next Hop field length malformed");
			}

			(bgpUpdate.NextHops ??= new List<IPAddress>()).Add(IPUtils.ReadIPAddress(buffer, IPType.IPv4));
		}
	}
}

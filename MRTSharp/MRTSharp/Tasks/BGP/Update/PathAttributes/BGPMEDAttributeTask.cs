﻿using MRTSharp.File;
using MRTSharp.Model.BGP;
using System;

namespace MRTSharp.Tasks.BGP.Update.PathAttributes
{
	internal class BGPMEDAttributeTask : IBGPAttributeTask
	{
		private const int MEDLength = 4;

		public void Run(FileBuffer buffer, ref BGPMessageUpdate bgpUpdate, UInt16 attributeLength, UInt16 flags)
		{
			if (attributeLength != MEDLength)
			{
				throw new Exception("MED field length malformed");
			}

			bgpUpdate.Med = buffer.ReadUInt32();
		}
	}
}

﻿using Microsoft.Extensions.Logging;
using MRTSharp.AppContext;
using MRTSharp.File;
using MRTSharp.Model.BGP;
using MRTSharp.Model.BGP.Constants;
using MRTSharp.Model.IP;
using MRTSharp.Tasks.BGP.Common;
using MRTSharp.Tasks.BGP.NLRI;
using MRTSharp.Utils;
using System;
using System.Collections.Generic;

namespace MRTSharp.Tasks.BGP.Update.PathAttributes
{
	internal abstract class BGPAbstractNLRIAttributeTask : IBGPAttributeTask
	{
		public void Run(FileBuffer buffer, ref BGPMessageUpdate bgpUpdate, UInt16 attributeLength, UInt16 flags)
		{
			Boolean skipNLRI = FlagUtils.GetSkipNLRI(flags);
			if (skipNLRI)
			{
				buffer.Seek(attributeLength);
				return;
			}

			(AFI, SAFI) afiSafi = BGPAFISAFITask.Run(buffer, ref attributeLength);
			try
			{
				ParseNextHops(buffer, ref bgpUpdate, ref attributeLength, afiSafi, flags);

				ParseNLRI(buffer, ref bgpUpdate, ref attributeLength, afiSafi, flags);
			}
			catch
			{
				Singleton<ApplicationContext>.Instance.Logger?.LogDebug("AFI/SAFI={afiSafi} not supported", afiSafi);

				buffer.Seek(attributeLength);
			}
		}

		internal abstract void ParseNextHops(FileBuffer buffer, ref BGPMessageUpdate bgpUpdate, ref UInt16 attributeLength, (AFI afi, SAFI safi) afiSafi, UInt16 flags);

		internal void ParseNLRI(FileBuffer buffer, ref BGPMessageUpdate bgpUpdate, ref UInt16 attributeLength, (AFI afi, SAFI safi) afiSafi, UInt16 flags)
		{
			List<IPPrefix> nlriList = GetNLRIList(ref bgpUpdate, attributeLength);

			switch (afiSafi)
			{
				/* Add here other supported AFIs */
				case (AFI.IPv4, SAFI.UNICAST):
					BGPIPUnicastNLRITask.ParseNLRI(IPType.IPv4, buffer, ref bgpUpdate, ref nlriList, ref attributeLength, flags);

					break;
				case (AFI.IPv6, SAFI.UNICAST):
					BGPIPUnicastNLRITask.ParseNLRI(IPType.IPv6, buffer, ref bgpUpdate, ref nlriList, ref attributeLength, flags);

					break;
				default:
					/* AFI/SAFI Not supported */
					throw new Exception($"AFI/SAFI={afiSafi} not supported");
			}
		}

		internal abstract List<IPPrefix> GetNLRIList(ref BGPMessageUpdate bgpUpdate, UInt16 attributeLength);
	}
}

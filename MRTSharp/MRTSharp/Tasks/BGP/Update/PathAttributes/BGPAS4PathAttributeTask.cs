﻿using MRTSharp.Model.BGP;
using MRTSharp.Model.BGP.PathAttributes;
using System;

namespace MRTSharp.Tasks.BGP.Update.PathAttributes
{
	internal class BGPAS4PathAttributeTask : BGPAbstractASPathAttributeTask
	{
		private const UInt32 AS_TRANS = 23456;

		public BGPAS4PathAttributeTask()
		{
			AttributeAsByteLength = 4;
		}

		internal override Boolean IsASPathValid(BGPMessageUpdate bgpUpdate, UInt16 attributeLength, ASPathSegmentType type, UInt16 asPathSegmentLength)
		{
			return base.IsASPathValid(bgpUpdate, attributeLength, type, asPathSegmentLength) &&
				bgpUpdate.ASPath.Exists(asEntry => Array.Exists(asEntry.ASNumbers, asNumberEntry => asNumberEntry == AS_TRANS));
		}
	}
}

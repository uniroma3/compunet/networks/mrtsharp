﻿using MRTSharp.File;
using MRTSharp.Model.BGP;
using MRTSharp.Model.BGP.PathAttributes;
using MRTSharp.Utils;
using System;
using System.Collections.Generic;

namespace MRTSharp.Tasks.BGP.Update.PathAttributes
{
	internal class BGPLargeCommunityAttributeTask : IBGPAttributeTask
	{
		private const int LargeCommunityLength = 12;

		public void Run(FileBuffer buffer, ref BGPMessageUpdate bgpUpdate, UInt16 attributeLength, UInt16 flags)
		{
			if (attributeLength % LargeCommunityLength != 0)
			{
				throw new Exception("Community field length malformed");
			}

			int nCommunities = attributeLength / LargeCommunityLength;
			if (nCommunities > 0)
			{
				bgpUpdate.Communities ??= new List<IBGPCommunity>(nCommunities);

				ReadOnlySpan<BGPLargeCommunity> communities = buffer.ReadAsType<BGPLargeCommunity>(attributeLength);

				for (int i = 0; i < nCommunities; ++i)
				{
					BGPLargeCommunity community = communities[i];

					community.ASNumber = ByteUtils.Swap(community.ASNumber);
					community.LocalDataPart1 = ByteUtils.Swap(community.LocalDataPart1);
					community.LocalDataPart2 = ByteUtils.Swap(community.LocalDataPart2);

					bgpUpdate.Communities.Add(community);
				}
			}
		}
	}
}

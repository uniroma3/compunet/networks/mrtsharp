﻿using Microsoft.Extensions.Logging;
using MRTSharp.AppContext;
using MRTSharp.File;
using MRTSharp.Model.BGP4MP.Constants;
using MRTSharp.Model.MRT;
using MRTSharp.Model.MRT.Constants;
using MRTSharp.Model.TableDump.Constants;
using MRTSharp.Model.TableDump.Peer;
using MRTSharp.Tasks.BGP4MP.Message;
using MRTSharp.Tasks.BGP4MP.StateChange;
using MRTSharp.Tasks.TableDump;
using MRTSharp.Utils;
using System;

namespace MRTSharp.Tasks.MRT
{
	internal class MRTTask
	{
		private readonly FileBuffer _buffer;
		private TableDumpPeer[] _peerTable;

		public MRTTask(FileBuffer buffer)
		{
			_buffer = buffer;
		}

		internal void Start(Action<MRTEntry> callback)
		{
			while (_buffer.CheckAvailableLength())
			{
				MRTEntry entry = Run();

				if (callback is not null)
				{
					callback.Invoke(entry);
				}
			}
		}

		private MRTEntry Run()
		{
			MRTHeader header = MRTHeaderTask.Run(_buffer);

			return RunByType(header);
		}

		private MRTEntry RunByType(MRTHeader header)
		{
			MRTEntry mrtEntry;

			switch (header.GetMRTType())
			{
				case MRTType.BGP4MP:
				case MRTType.BGP4MP_ET:
					{
						BGP4MPSubtype subType = (BGP4MPSubtype)header.GetSubtype();

						mrtEntry = (subType) switch
						{
							BGP4MPSubtype.BGP4MP_STATE_CHANGE => Singleton<BGP4MPStateChangeTask>.Instance.Run(_buffer, subType),
							BGP4MPSubtype.BGP4MP_MESSAGE => Singleton<BGP4MPMessageTask>.Instance.Run(_buffer, subType),
							BGP4MPSubtype.BGP4MP_MESSAGE_AS4 => Singleton<BGP4MPMessageTask>.Instance.Run(_buffer, subType),
							BGP4MPSubtype.BGP4MP_STATE_CHANGE_AS4 => Singleton<BGP4MPStateChangeTask>.Instance.Run(_buffer, subType),
							BGP4MPSubtype.BGP4MP_MESSAGE_LOCAL => Singleton<BGP4MPMessageTask>.Instance.Run(_buffer, subType),
							BGP4MPSubtype.BGP4MP_MESSAGE_AS4_LOCAL => Singleton<BGP4MPMessageTask>.Instance.Run(_buffer, subType),
							BGP4MPSubtype.BGP4MP_MESSAGE_ADDPATH => Singleton<BGP4MPMessageTask>.Instance.Run(_buffer, subType),
							BGP4MPSubtype.BGP4MP_MESSAGE_AS4_ADDPATH => Singleton<BGP4MPMessageTask>.Instance.Run(_buffer, subType),
							BGP4MPSubtype.BGP4MP_MESSAGE_LOCAL_ADDPATH => Singleton<BGP4MPMessageTask>.Instance.Run(_buffer, subType),
							BGP4MPSubtype.BGP4MP_MESSAGE_AS4_LOCAL_ADDPATH => Singleton<BGP4MPMessageTask>.Instance.Run(_buffer, subType),
							_ => throw new Exception($"BGP4MP Message with subtype code {subType} not defined"),
						};

						break;
					}
				case MRTType.TABLE_DUMP_V2:
					{
						TableDumpSubtype subType = (TableDumpSubtype)header.GetSubtype();

						switch (subType)
						{
							case TableDumpSubtype.PEER_INDEX_TABLE:
								/* Parse the Peer Index Table and store it in this instance */
								_peerTable = TableDumpPeerIndexTableTask.Run(_buffer);
								return null;
							case TableDumpSubtype.RIB_IPV4_UNICAST:
							case TableDumpSubtype.RIB_IPV6_UNICAST:
							case TableDumpSubtype.RIB_IPV4_UNICAST_ADDPATH:
							case TableDumpSubtype.RIB_IPV6_UNICAST_ADDPATH:
								if (_peerTable is null)
								{
									throw new Exception("PEER_INDEX_TABLE not found");
								}

								mrtEntry = TableDumpRIBTask.Run(_buffer, subType, _peerTable);
								break;
							default:
								Singleton<ApplicationContext>.Instance.Logger?.LogDebug("TABLE_DUMP_V2 Message with subtype code {subType} not supported", subType);

								_buffer.Seek(header.GetLength());

								return null;
						};

						break;
					}
				default:
					throw new Exception("MRT Message with type code " + header.GetMRTType() + " not supported");
			}

			mrtEntry.OriginateTime = header.GetTimestamp();

			return mrtEntry;
		}
	}
}

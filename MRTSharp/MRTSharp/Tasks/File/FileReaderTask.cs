﻿using ICSharpCode.SharpZipLib.BZip2;
using MRTSharp.File;
using System;
using System.IO;
using System.IO.Compression;
using System.Text.RegularExpressions;

namespace MRTSharp.Tasks.File
{
	internal static class FileReaderTask
	{
		private static readonly Regex _numericRegex = new(@"^\.[0-9]+$");
		internal static FileBuffer Run(FileInfo file)
		{
			return file.Extension switch
			{
				".gz" => LoadGZ(file),
				".bz2" => LoadBZ2(file),
				"" => LoadBinary(file),
				_ => new Func<FileBuffer>(() =>
				{
					/* Match uncompressed files from Route Collectors */
					if (_numericRegex.IsMatch(file.Extension))
					{
						return LoadBinary(file);
					}

					throw new Exception($"File extension {file.Extension} not supported");
				}).Invoke()
			};
		}

		private static FileBuffer LoadGZ(FileInfo file)
		{
			using (FileStream fileStream = new(file.FullName, FileMode.Open, FileAccess.Read, FileShare.Read))
			{
				fileStream.Position = fileStream.Length - 4;
				var lengthByteArray = new byte[4];
				fileStream.Read(lengthByteArray, 0, 4);
				uint fileLength = BitConverter.ToUInt32(lengthByteArray, 0);
				fileStream.Position = 0;

				using (GZipStream gzStream = new(fileStream, CompressionMode.Decompress))
				{
					return FileBuffer.FromStream(gzStream, length: fileLength);
				}
			}
		}

		private static FileBuffer LoadBZ2(FileInfo file)
		{
			using (FileStream fileStream = new(file.FullName, FileMode.Open, FileAccess.Read, FileShare.Read))
			{
				using (BZip2InputStream bzStream = new(fileStream))
				{
					return FileBuffer.FromStream(bzStream);
				}
			}
		}

		private static FileBuffer LoadBinary(FileInfo file)
		{
			using (FileStream fileStream = new(file.FullName, FileMode.Open, FileAccess.Read, FileShare.Read))
			{
				return FileBuffer.FromStream(fileStream, length: fileStream.Length);
			}
		}
	}
}

﻿using MRTSharp.File;
using MRTSharp.Model.BGP;
using MRTSharp.Model.BGP.Constants;
using MRTSharp.Model.IP;
using MRTSharp.Model.TableDump;
using MRTSharp.Model.TableDump.Constants;
using MRTSharp.Model.TableDump.Peer;
using MRTSharp.Tasks.BGP.Update;
using MRTSharp.Utils;
using System;

namespace MRTSharp.Tasks.TableDump
{
	internal static class TableDumpRIBTask
	{
		internal static TableDumpRIBRecord Run(FileBuffer buffer, TableDumpSubtype subType, TableDumpPeer[] peerTable)
		{
			IPType ipType = (subType) switch
			{
				TableDumpSubtype.RIB_IPV4_UNICAST or TableDumpSubtype.RIB_IPV4_UNICAST_ADDPATH => IPType.IPv4,
				TableDumpSubtype.RIB_IPV6_UNICAST or TableDumpSubtype.RIB_IPV6_UNICAST_ADDPATH => IPType.IPv6,
				_ => throw new Exception("Table Dump Subtype invalid")
			};

			AFI afi = (ipType) switch
			{
				IPType.IPv4 => AFI.IPv4,
				IPType.IPv6 => AFI.IPv6,
				_ => throw new Exception("Table Dump AFI invalid")
			};
			SAFI safi = SAFI.UNICAST;

			/* Skip Sequence Number */
			buffer.Seek(sizeof(UInt32));

			IPPrefix prefix = IPUtils.ReadPrefix(buffer, ipType);
			UInt16 nEntries = buffer.ReadUInt16();

			TableDumpRIBRecord ribRecord = new TableDumpRIBRecord(subType, prefix);

			for (int i = 0; i < nEntries; ++i)
			{
				BGPMessageUpdate bgpUpdate = new BGPMessageUpdate();

				TableDumpRIBEntry ribEntry = new TableDumpRIBEntry(
					peer: peerTable[buffer.ReadUInt16()],
					originateTime: DateTime.UnixEpoch.AddSeconds(buffer.ReadUInt32()),
					bgpUpdate
				);
				ribRecord.Entries.Add(ribEntry);

				if (ribRecord.HasAddPath())
				{
					ribEntry.PathId = buffer.ReadUInt32();
				}

				UInt16 messageLength = buffer.ReadUInt16();

				/* asByteLength is always 4 (see https://tools.ietf.org/html/rfc6396#section-4.3.4) */
				/* NLRI Attribute should trimmed in TABLE_DUMP, so we skip it if it is found */
				UInt16 flags = FlagUtils.GetFlags(asByteLength: 4, ribRecord.HasAddPath(), skipNLRI: true);
				BGP4MPMessageUpdateAttributesTask.ParseAttributes(buffer, ref bgpUpdate, messageLength, flags);
			}

			return ribRecord;
		}
	}
}

﻿using MRTSharp.File;
using MRTSharp.Model.IP;
using MRTSharp.Model.TableDump.Constants;
using MRTSharp.Model.TableDump.Peer;
using MRTSharp.Utils;
using System;

namespace MRTSharp.Tasks.TableDump
{
	internal static class TableDumpPeerIndexTableTask
	{
		internal static TableDumpPeer[] Run(FileBuffer buffer)
		{
			/* Skip the Collector BGP ID */
			buffer.Seek(sizeof(UInt32));

			UInt16 viewNameLength = buffer.ReadUInt16();
			/* Skip the View Name */
			buffer.Seek(viewNameLength);

			UInt16 peerCount = buffer.ReadUInt16();
			TableDumpPeer[] peerTable = new TableDumpPeer[peerCount];

			for (int i = 0; i < peerCount; ++i)
			{
				TableDumpPeerType peerType = (TableDumpPeerType)buffer.ReadByte();

				/* Skip Peer BGP ID */
				buffer.Seek(sizeof(UInt32));

				IPType ipType = (peerType) switch
				{
					TableDumpPeerType.IPV4_2BYTE or TableDumpPeerType.IPV4_4BYTE => IPType.IPv4,
					TableDumpPeerType.IPV6_2BYTE or TableDumpPeerType.IPV6_4BYTE => IPType.IPv6,
					_ => throw new Exception("Peer Size invalid")
				};
				Func<UInt32> asByteLength = (peerType) switch
				{
					TableDumpPeerType.IPV4_2BYTE or TableDumpPeerType.IPV6_2BYTE => () => buffer.ReadUInt16(),
					TableDumpPeerType.IPV4_4BYTE or TableDumpPeerType.IPV6_4BYTE => () => buffer.ReadUInt32(),
					_ => throw new Exception("Peer Size invalid")
				};

				peerTable[i] = new TableDumpPeer()
				{
					IpAddress = IPUtils.ReadIPAddress(buffer, ipType),
					PeerAS = asByteLength()
				};
			}

			return peerTable;
		}
	}
}

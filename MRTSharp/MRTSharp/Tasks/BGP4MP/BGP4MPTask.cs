﻿using MRTSharp.File;
using MRTSharp.Model.BGP.Constants;
using MRTSharp.Model.BGP4MP;
using MRTSharp.Model.BGP4MP.Constants;
using MRTSharp.Model.BGP4MP.Peer;
using MRTSharp.Model.IP;
using System;

namespace MRTSharp.Tasks.BGP4MP
{
	internal abstract class BGP4MPTask
	{
		public BGP4MPEntry Run(FileBuffer buffer, BGP4MPSubtype subType)
		{
			Byte asLength = subType.GetASLength();

			/* Skip ASes and Interface Index */
			buffer.Seek((asLength * 2) + sizeof(UInt16));
			AFI afi = (AFI)buffer.ReadUInt16();
			/* Go before ASes */
			buffer.Seek(-(asLength * 2) - sizeof(UInt16) - sizeof(UInt16));

			IPType ipType = (IPType)afi;
			if(!Enum.IsDefined(ipType))
			{
				ipType = IPType.Null;
			}

			BGPPeer peers = PeerTask.Run(buffer, asLength, ipType);

			BGP4MPEntry bgp4MP = RunSpecific(buffer, subType);

			if (bgp4MP is not null)
			{
				bgp4MP.Peers = peers;
			}

			return bgp4MP;
		}

		protected abstract BGP4MPEntry RunSpecific(FileBuffer buffer, BGP4MPSubtype subType);
	}
}

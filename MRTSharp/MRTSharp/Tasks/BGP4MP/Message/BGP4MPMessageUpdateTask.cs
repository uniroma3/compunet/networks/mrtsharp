﻿using MRTSharp.File;
using MRTSharp.Model.BGP;
using MRTSharp.Model.BGP4MP;
using MRTSharp.Model.IP;
using MRTSharp.Tasks.BGP.Update;
using MRTSharp.Utils;
using System;
using System.Collections.Generic;

namespace MRTSharp.Tasks.BGP4MP.Message
{
	internal static class BGP4MPMessageUpdateTask
	{
		internal static void Run(FileBuffer buffer, UInt16 messageLength, BGP4MPMessage bgp4MPMessage)
		{
			BGPMessageUpdate bgpUpdate = new();
			bgp4MPMessage.Message = bgpUpdate;

			UInt16 flags = FlagUtils.GetFlags(bgp4MPMessage.GetASLength(), bgp4MPMessage.HasAddPath(), skipNLRI: false);

			BGP4MPMessageUpdateWithdrawalsTask.Run(buffer, ref bgpUpdate, ref messageLength, flags);

			BGP4MPMessageUpdateAttributesTask.Run(buffer, ref bgpUpdate, ref messageLength, flags);

			if (messageLength > 0)
			{
				if (bgpUpdate.Announcements is null)
				{
					bgpUpdate.Announcements = new List<IPPrefix>();
				}

				BGP4MPMessageUpdatePrefixesTask.Run(buffer, bgpUpdate.Announcements, ref messageLength, IPType.IPv4, flags);
			}
		}
	}
}

﻿using MRTSharp.Model.BGP;
using MRTSharp.Model.BGP4MP;
using System;

namespace MRTSharp.Tasks.BGP4MP.Message
{
	internal static class BGP4MPMessageKeepAliveTask
	{
		private const uint keepAliveMessageLength = 0;

		internal static void Run(UInt16 messageLength, BGP4MPMessage bgp4MPMessage)
		{
			if (messageLength != keepAliveMessageLength)
			{
				throw new Exception("KEEP_ALIVE message malformed");
			}

			bgp4MPMessage.Message = new BGPMessageKeepAlive();
		}
	}
}

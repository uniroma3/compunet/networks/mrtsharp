using MRTSharp.File;
using MRTSharp.Model.BGP.Constants;
using MRTSharp.Model.BGP4MP;
using MRTSharp.Model.BGP4MP.Constants;
using System;

namespace MRTSharp.Tasks.BGP4MP.Message
{
	internal class BGP4MPMessageTask : BGP4MPTask
	{
		private const ushort securitySkip = 16;

		protected override BGP4MPEntry RunSpecific(FileBuffer buffer, BGP4MPSubtype subType)
		{
			buffer.Seek(securitySkip);

			UInt16 messageLength = buffer.ReadUInt16();
			BGPMessageType messageType = (BGPMessageType)buffer.ReadByte();

			/* 16 bytes skipped + 2 byte message length + 1 byte message type */
			messageLength -= (securitySkip + sizeof(UInt16) + sizeof(byte));

			BGP4MPMessage bgp4MPMessage = new(subType);
			switch(messageType) {
				case BGPMessageType.OPEN:
					BGP4MPMessageOpenTask.Run(buffer, bgp4MPMessage);
					break;
				case BGPMessageType.UPDATE: 
					BGP4MPMessageUpdateTask.Run(buffer, messageLength, bgp4MPMessage);
					break;
				case BGPMessageType.NOTIFICATION:
					BGP4MPMessageNotificationTask.Run(buffer, messageLength, bgp4MPMessage);
					break;
				case BGPMessageType.KEEPALIVE:
					BGP4MPMessageKeepAliveTask.Run(messageLength, bgp4MPMessage);
					break;
				case BGPMessageType.ROUTE_REFRESH:
					BGP4MPMessageRouteRefreshTask.Run(buffer, messageLength, bgp4MPMessage);
					break;
				default:
					throw new Exception($"BGP Message Type {messageType} not defined");
			};
				
			return bgp4MPMessage;
		}
	}
}

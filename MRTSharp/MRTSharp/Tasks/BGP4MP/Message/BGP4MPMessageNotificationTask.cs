﻿using MRTSharp.File;
using MRTSharp.Model.BGP;
using MRTSharp.Model.BGP.Constants;
using MRTSharp.Model.BGP4MP;
using System;

namespace MRTSharp.Tasks.BGP4MP.Message
{
	internal static class BGP4MPMessageNotificationTask
	{
		internal static void Run(FileBuffer buffer, UInt16 messageLength, BGP4MPMessage bgp4MPMessage)
		{
			bgp4MPMessage.Message = new BGPMessageNotification()
			{
				ErrorCode = (BGPErrorCode)buffer.ReadByte(),
				ErrorSubCode = buffer.ReadByte()
			};

			messageLength -= (sizeof(Byte) + sizeof(Byte));

			buffer.Seek(messageLength);
		}
	}
}

﻿using MRTSharp.File;
using MRTSharp.Model.BGP;
using MRTSharp.Model.BGP.Constants;
using MRTSharp.Model.BGP4MP;
using MRTSharp.Tasks.BGP.Common;
using System;

namespace MRTSharp.Tasks.BGP4MP.Message
{
	internal static class BGP4MPMessageRouteRefreshTask
	{
		private const uint routeRefreshMessageLength = 4;

		internal static void Run(FileBuffer buffer, UInt16 messageLength, BGP4MPMessage bgp4MPMessage)
		{
			if (messageLength != routeRefreshMessageLength)
			{
				throw new Exception("ROUTE_REFRESH message malformed");
			}

			(AFI afi, SAFI safi) = BGPAFISAFITask.Run(buffer, ref messageLength, skipReserved: true);

			bgp4MPMessage.Message = new BGPMessageRouteRefresh()
			{
				Afi = afi,
				Safi = safi
			};
		}
	}
}

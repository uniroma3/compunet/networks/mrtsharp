using MRTSharp.File;
using MRTSharp.Model.BGP.Constants;
using MRTSharp.Model.BGP4MP;
using MRTSharp.Model.BGP4MP.Constants;

namespace MRTSharp.Tasks.BGP4MP.StateChange
{
	internal class BGP4MPStateChangeTask : BGP4MPTask
	{
		protected override BGP4MPEntry RunSpecific(FileBuffer buffer, BGP4MPSubtype subType)
		{
			BGP4MPStateChange bgpStateChange = new BGP4MPStateChange(subType)
			{
				OldState = (BGPStateStatus)buffer.ReadUInt16(),
				NewState = (BGPStateStatus)buffer.ReadUInt16()
			};

			return bgpStateChange;
		}
	}
}

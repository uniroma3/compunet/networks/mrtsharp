﻿using MRTSharp.Model.BGP.Constants;
using System;


namespace MRTSharp.Exceptions
{
	public class CapabilityLenghtMalformed : Exception
	{
		public BGPCapability Capability { get; set; }
		public string Expected { get; set; }
		public int Got { get; set; }

		public CapabilityLenghtMalformed(BGPCapability capability, int expectedLenght, int gotLenght) :
			this(capability, expectedLenght.ToString(), gotLenght)
		{ }

		public CapabilityLenghtMalformed(BGPCapability capability, string expectedLenght, int gotLenght) :
			base($"{capability} capability length malformed. Expected {expectedLenght}, got {gotLenght}")
		{
			Capability= capability;
			Expected= expectedLenght;
			Got= gotLenght;
		}
	}
}
